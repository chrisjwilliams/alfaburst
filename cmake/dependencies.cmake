# thirdparty dependencies
include(cmake/googletest.cmake)


find_package(Panda REQUIRED)
list(APPEND CMAKE_MODULE_PATH "${PANDA_CMAKE_MODULE_PATH}")
include(${PANDA_CMAKE_MODULE_PATH}/cuda.cmake)
include(cmake/boost.cmake)

find_package(Cheetah REQUIRED)
find_package(FFTW REQUIRED) # required by cheetah
include(cmake/hiredis.cmake)

include_directories(SYSTEM ${CHEETAH_INCLUDE_DIR} ${Boost_INCLUDE_DIR} ${PANDA_INCLUDE_DIR} ${FFTW_INCLUDES})
set(DEPENDENCY_LIBRARIES
    ${CHEETAH_LIBRARIES}
    ${PANDA_LIBRARIES}
    ${Boost_LIBRARIES}
    ${FFTW_LIBRARIES}
    ${HIREDIS_LIBRARIES}
    ${CUDA_LIBRARIES}
)

include(compiler_settings)
include(cmake/cuda.cmake)

