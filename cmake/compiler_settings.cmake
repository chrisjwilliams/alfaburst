#
# Compiler defaults for pelican.
# This file is included in the top level pelican CMakeLists.txt.
#

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "release")
endif ()

# Set compiler flags
set(CMAKE_CXX_FLAGS "-fPIC")

if(CMAKE_CXX_COMPILER MATCHES icpc)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wcheck -wd2259 -wd1125")
endif()
if (CMAKE_CXX_COMPILER_ID MATCHES Clang)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Werror")
    set(CMAKE_CXX_FLAGS_PROFILE "-g -O0 -pg -fprofile-arcs -ftest-coverage")
endif ()
if (CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=c++11 -Werror -pthread")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wcast-align")
    set(CMAKE_CXX_FLAGS_PROFILE "-g -O0 -pg -fprofile-arcs -ftest-coverage")
endif ()

set(CMAKE_CXX_FLAGS_RELEASE "-O3 -DNDEBUG")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g -Wall -Wextra -pedantic -mtune=native")
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g -Wall -Wextra -pedantic -march=native -mtune=native")

# Set include directories for dependencies
include_directories(
    ${Boost_INCLUDE_DIRS}
)
