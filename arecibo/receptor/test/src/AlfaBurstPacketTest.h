#ifndef ARECIBO_ALFABURST_TEST_ALFABURSTPACKETTEST_H
#define ARECIBO_ALFABURST_TEST_ALFABURSTPACKETTEST_H

#include <gtest/gtest.h>

namespace arecibo {
namespace alfaburst {
namespace test {

/**
 * @brief
 * 
 * @details
 * 
 */
class AlfaBurstPacketTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        AlfaBurstPacketTest();

        ~AlfaBurstPacketTest();

    private:
};

} // namespace test
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_TEST_ALFABURSTPACKETTEST_H 
