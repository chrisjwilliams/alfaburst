#include "UdpStreamTraitsTest.h"
#include "arecibo/receptor/UdpStreamTraits.h"
#include "panda/detail/packet_stream/ChunkerContext.h"
#include <memory>
#include <functional>


namespace arecibo {
namespace alfaburst {
namespace receptor {
namespace test {


TYPED_TEST(UdpStreamTraitsTest, test_sequence_number)
{
    AlfaBurstPacket packet;
    for(uint64_t i=0; i < (std::numeric_limits<uint64_t>::max() & 0xffffff); i+=1024) {
        packet.set_packet_count(i);
        ASSERT_EQ( 4 * i, UdpStreamTraits<TypeParam>::sequence_number(packet));
    }
}

TYPED_TEST(UdpStreamTraitsTest, test_chunk_size)
{
    SampleCastConfig config;
    UdpStreamTraits<TypeParam> traits(config);
    typename UdpStreamTraits<TypeParam>::DataType data(ska::cheetah::data::DimensionSize<ska::cheetah::data::Frequency>(32)
                                                      ,ska::cheetah::data::DimensionSize<ska::cheetah::data::Time>(100));
    ASSERT_EQ(3200U, traits.chunk_size(data));
}

template<typename TypeParam>
struct Context
{
    public:
        typedef typename UdpStreamTraits<TypeParam>::DataType DataType;

    public:
        typedef ska::panda::packet_stream::ChunkerContext<UdpStreamTraits<TypeParam>, std::function<std::shared_ptr<DataType>(DataType const&)>> ContextType;
        typedef std::shared_ptr<ContextType> ContextPtrType;

        Context(unsigned number_of_samples, unsigned number_of_channels)
            : _number_of_samples(number_of_samples)
            , _number_of_channels(number_of_channels)
            , _offset(0)
        {
        }
        Context(Context&) = delete;

        std::size_t offset() {
            auto offset = _offset;
            _offset += _number_of_channels;
            return offset;
        }

        DataType& chunk()
        {
            if(!_data) {
                _data.reset(new DataType(_number_of_samples, _number_of_channels));
            }
            return *_data;
        }

        unsigned packets_per_chunk() {
            return (_number_of_samples * _number_of_channels) * sizeof(uint16_t)/ AlfaBurstPacket::payload_size();
        }

        std::size_t packet_offset() {
            return 0;
        }

        std::size_t size() {
            return 0;
        }

    private:
        std::shared_ptr<DataType> _data;
        data::DimensionSize<data::Time> _number_of_samples;
        data::DimensionSize<data::Frequency> _number_of_channels;
        std::size_t _offset;
        std::shared_ptr<Context> _next;
};

template<typename TypeParam>
std::ostream& operator<<(std::ostream& os, Context<TypeParam> const&) { return os;}

TYPED_TEST(UdpStreamTraitsTest, test_deserialise_packet)
{
    SampleCastConfig config;
    Context<TypeParam> context(1000, 77);
    UdpStreamTraits<TypeParam> traits(config);
    AlfaBurstPacket packet;

    auto packets_per_chunk=traits.chunk_size(context.chunk())/traits.packet_size();
    for(unsigned i = 0; i < packets_per_chunk; ++i) {
        traits.template deserialise_packet<Context<TypeParam>>(context, packet);
    }
}

} // namespace test
} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
