#include "AlfaBurstPacketTest.h"
#include "arecibo/receptor/AlfaBurstPacket.h"


namespace arecibo {
namespace alfaburst {
namespace test {


AlfaBurstPacketTest::AlfaBurstPacketTest()
    : ::testing::Test()
{
}

AlfaBurstPacketTest::~AlfaBurstPacketTest()
{
}

void AlfaBurstPacketTest::SetUp()
{
}

void AlfaBurstPacketTest::TearDown()
{
}

TEST_F(AlfaBurstPacketTest, test_packet_count)
{
    AlfaBurstPacket packet;
    packet.set_spectral_quarter(2); // non zero
    packet.set_beam(3); // non zero

    // min
    packet.set_packet_count(0);
    ASSERT_EQ(0x0000000000000000U, packet.packet_count());

    packet.set_packet_count(1);
    ASSERT_EQ(0x0000000000000001U, packet.packet_count());

    // max
    packet.set_packet_count(0x0000FFFFFFFFFFFF);
    ASSERT_EQ(0x0000FFFFFFFFFFFFU, packet.packet_count());

    // > max
    packet.set_packet_count(0x0001000000000000);
    ASSERT_EQ(0x0000000000000000U, packet.packet_count());
}

TEST_F(AlfaBurstPacketTest, test_number_of_samples)
{
    AlfaBurstPacket packet;
    ASSERT_EQ(1024U, packet.number_of_samples());
    ASSERT_EQ(packet.payload_size(), sizeof(AlfaBurstPacket::Sample) * packet.number_of_samples());
}

TEST_F(AlfaBurstPacketTest, test_beam)
{
    AlfaBurstPacket packet;
    for(uint8_t i=0; i< 4; ++i) {
        packet.set_beam(i);
        ASSERT_EQ(i, packet.beam());
    }
}

TEST_F(AlfaBurstPacketTest, test_spectral_quarter)
{
    AlfaBurstPacket packet;
    for(uint8_t i=0; i< 4; ++i) {
        packet.set_spectral_quarter(i);
        ASSERT_EQ(i, packet.spectral_quarter());
    }
}

TEST_F(AlfaBurstPacketTest, test_insert)
{
    AlfaBurstPacket packet;
    packet.set_packet_count(1);
    packet.set_beam(2);
    for(std::size_t i=0; i< packet.number_of_samples(); ++i) {
        packet.insert(i, AlfaBurstPacket::Sample(i, 2*i, 3*i, 4*i));
        AlfaBurstPacket::Sample const& sample = packet.sample(i);
        ASSERT_EQ(static_cast<short>(i),sample.xx());
        ASSERT_EQ(static_cast<short>(i*2),sample.yy());
        ASSERT_EQ(static_cast<short>(i*3),sample.xy_re());
        ASSERT_EQ(static_cast<short>(i*4),sample.xy_im());
        // check we didn't overwrite the header
        ASSERT_EQ(0x0000000000000001U, packet.packet_count()) << i;
        ASSERT_EQ(2, packet.beam()) << i;
    }
}

TEST_F(AlfaBurstPacketTest, size)
{
    AlfaBurstPacket packet;
    ASSERT_EQ(packet.size(), sizeof(AlfaBurstPacket));
}

} // namespace test
} // namespace alfaburst
} // namespace arecibo
