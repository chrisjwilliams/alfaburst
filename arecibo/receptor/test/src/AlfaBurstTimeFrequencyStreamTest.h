#ifndef ARECIBO_ALFABURST_TEST_ALFABURSTTIMEFREQUENCYSTREAMTEST_H
#define ARECIBO_ALFABURST_TEST_ALFABURSTTIMEFREQUENCYSTREAMTEST_H

#include "arecibo/receptor/Config.h"
#include <gtest/gtest.h>

namespace arecibo {
namespace alfaburst {
namespace receptor {
namespace test {

/**
 * @brief
 * 
 * @details
 * 
 */
class AlfaBurstTimeFrequencyStreamTest : public ::testing::Test
{
    protected:
        typedef AlfaBurstTimeFrequencyStreamTest SelfType;

    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        AlfaBurstTimeFrequencyStreamTest();
        ~AlfaBurstTimeFrequencyStreamTest();

        receptor::Config& config();

    private:
};

} // namespace test
} // namespace receptor
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_TEST_ALFABURSTTIMEFREQUENCYSTREAMTEST_H 
