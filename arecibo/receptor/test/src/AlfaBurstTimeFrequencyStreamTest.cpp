#include "AlfaBurstTimeFrequencyStreamTest.h"
#include "arecibo/receptor/AlfaBurstTimeFrequencyStream.h"
#include "arecibo/utils/RedisConfig.h"
#include "arecibo/emulator/Emulator.h"
#include <deque>
#include <algorithm>


namespace arecibo {
namespace alfaburst {
namespace receptor {
namespace test {


AlfaBurstTimeFrequencyStreamTest::AlfaBurstTimeFrequencyStreamTest()
    : ::testing::Test()
{
}

AlfaBurstTimeFrequencyStreamTest::~AlfaBurstTimeFrequencyStreamTest()
{
}

receptor::Config& AlfaBurstTimeFrequencyStreamTest::config()
{
    static receptor::Config config;
    static RedisConfig redis_config;
    redis_config.address(boost::asio::ip::address());
    config.redis_config(redis_config);
    return config;
}

void AlfaBurstTimeFrequencyStreamTest::SetUp()
{
}

void AlfaBurstTimeFrequencyStreamTest::TearDown()
{
}

struct TestModel : public ska::cheetah::generators::TimeFrequencyGenerator<uint16_t>{
    public:
        typedef typename receptor::UdpStreamTraits<uint16_t>::DataType DataType;
        TestModel() : _val(0) {}

        void next(DataType& data) override {
            // fill data with values
            std::lock_guard<std::mutex> lk(_mutex);
            std::generate(data.begin(), data.end(), [&]{ return ++_val; } );
            PANDA_LOG << " model next (" << &data << ") val=" << _val << " data_size=" << data.data_size();
            _data.push_back(data);
        }

        DataType sent_data() {
            std::unique_lock<std::mutex> lk(_mutex);
            assert(_data.size() > 0);
            DataType d =  _data.front();
            _data.pop_front();
            return d;
        }

    private:
        std::mutex _mutex;
        std::deque<DataType> _data;
        typename DataType::DataType _val;
};

template<typename NumericalRep, typename TestClass>
void data_consistency_test(TestClass& test)
{
    typedef receptor::AlfaBurstTimeFrequencyStreamProducer<NumericalRep> Producer;

    std::size_t number_of_channels = 4 * 1024; //  sent in 4 seperate packets
    std::size_t number_of_time_samples = 100;

    // Producer
    receptor::Config& config = test.config();
    config.number_of_time_samples(number_of_time_samples);
    config.number_of_threads(2);
    Producer stream(config);
    ska::panda::DataManager<Producer> dm(stream);

    // start up the emulator
    ska::cheetah::emulator::Config emulator_settings;
    emulator_settings.number_of_channels(number_of_channels);
    emulator_settings.fixed_address(ska::panda::IpAddress(stream.local_end_point()));
    TestModel* model = new TestModel();
    Emulator emulator(emulator_settings, model); // takes ownership of model
    std::thread emulator_thread([&]() { emulator.run(); });

    // find where sent and received sequences match
    std::shared_ptr<data::TimeFrequency<Cpu, NumericalRep>> data_out = std::get<0>(dm.next());
    auto it = data_out->cbegin();

    SampleCastConfig sample_cast_config;
    SampleCast<NumericalRep> sample_cast(sample_cast_config);

    auto sent_data=model->sent_data();
    auto sent_it=sent_data.cbegin();

    while(sample_cast(*sent_it) != *it) {
        if(++sent_it == sent_data.end()) {
            sent_data=model->sent_data();
            sent_it=sent_data.cbegin();
        }
    }
    PANDA_LOG << "Packets aligned at value: " << *sent_it << " = " << *it;

    try {
        for(unsigned i=0; i < 3; ++i) { // number of blocks to test
            SCOPED_TRACE(i);
            SCOPED_TRACE("testing data block:");
            ASSERT_EQ(number_of_channels, data_out->number_of_channels()) << " chunk=" << (data_out);
            ASSERT_EQ(number_of_time_samples, data_out->number_of_spectra());
            typedef typename Producer::FrequencyType FrequencyType;
            FrequencyType fch(1599.0 * boost::units::si::mega * ska::cheetah::data::hertz + 2048.0*(448.0/4096.0 * boost::units::si::mega * ska::cheetah::data::hertz));
            FrequencyType foff(-224.0/4096.0 * boost::units::si::mega * ska::cheetah::data::hertz);
            ASSERT_EQ(data_out->channel_frequencies().size(), number_of_channels);

            unsigned chan=0;
            for(auto const& f : data_out->channel_frequencies()) {
                ASSERT_EQ((fch + FrequencyType( chan * foff)), f) << "channel index=" << chan << " fch=" << fch << " foff=" << foff;
                ++chan;
            }

            std::size_t count=0;
            while(it != data_out->cend() ) {
                if(sent_it == sent_data.cend())
                {
                    sent_data=model->sent_data();
                    sent_it=sent_data.cbegin();
                    ASSERT_FALSE(sent_it == sent_data.cend());
                }
                //if(*it != 0) // attempt to cater for slow tests where missing packets might happen
                //{
                    ASSERT_EQ(sample_cast(*sent_it), *it) << " count=" << count << " chunk=" << data_out;
                //}
                ++count;
                ++it;
                ++sent_it;
            }
            data_out = std::get<0>(dm.next());
            it = data_out->cbegin();
            ASSERT_NE(it, data_out->cend());
        }
    }
    catch(...) {
    }

    // close down the emulator
    emulator.stop();
    emulator_thread.join();
}

TEST_F(AlfaBurstTimeFrequencyStreamTest, emulator_test_uint16_t)
{
    data_consistency_test<uint16_t, SelfType>(*this);
}

TEST_F(AlfaBurstTimeFrequencyStreamTest, emulator_test_uint8_t)
{
    data_consistency_test<uint8_t, SelfType>(*this);
}

} // namespace test
} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
