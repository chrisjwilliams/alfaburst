#ifndef ARECIBO_ALFABURST_TEST_UDPSTREAMTRAITSTEST_H
#define ARECIBO_ALFABURST_TEST_UDPSTREAMTRAITSTEST_H

#include <gtest/gtest.h>

namespace arecibo {
namespace alfaburst {
namespace receptor {
namespace test {

/**
 * @brief
 * 
 * @details
 * 
 */
template<typename T>
class UdpStreamTraitsTest : public ::testing::Test
{
    public:
        UdpStreamTraitsTest()
            : ::testing::Test()
        {
        }
        ~UdpStreamTraitsTest() {}

    private:
};

typedef ::testing::Types<uint8_t, uint16_t> MyTypes;
TYPED_TEST_CASE(UdpStreamTraitsTest, MyTypes);

} // namespace test
} // namespace receptor
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_TEST_UDPSTREAMTRAITSTEST_H 
