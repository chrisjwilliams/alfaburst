#ifndef ARECIBO_ALFABURST_RECEPTOR_UDPSTREAMTRAITS_H
#define ARECIBO_ALFABURST_RECEPTOR_UDPSTREAMTRAITS_H

#include "arecibo/data/TimeFrequency.h"
#include "arecibo/receptor/AlfaBurstPacketInspector.h"
#include "arecibo/receptor/detail/SampleCast.h"

namespace arecibo {
namespace alfaburst {
namespace receptor {

/**
 * @brief
 *    class to define the traits for the Udp TimeFrequency data stream conplying to interface for use by panda::PacketStream
 *
 */
class UdpStreamTraitsBase
{
    public:
        typedef AlfaBurstPacketInspector PacketInspector;

    public:
        UdpStreamTraitsBase();
        ~UdpStreamTraitsBase();

        static uint64_t sequence_number(PacketInspector const& packet);
        static constexpr uint64_t max_sequence_number();

        static unsigned packet_size();

        template<typename ContextType>
        void process_missing_slice(ContextType& context, std::size_t sample_num=0);


        void packet_stats(uint64_t received, uint64_t expected_packets);

        static unsigned channels_per_quarter();

        bool align_packet(PacketInspector const&) const;

};

template<typename TimeFrequencyNumericType>
class UdpStreamTraits : public UdpStreamTraitsBase
{
    public:
        typedef SampleCastConfig const& Init;

    public:
        typedef data::TimeFrequency<Cpu, TimeFrequencyNumericType> DataType;

    public:
        UdpStreamTraits(SampleCastConfig const& config);

        template<typename ContextType>
        void deserialise_packet(ContextType& context, PacketInspector const&);

        template<typename ContextType>
        void resize_chunk(ContextType& context);

        static unsigned chunk_size(DataType const&);

        void set(SampleCastConfig const&);

    private:
        SampleCast<TimeFrequencyNumericType> _sample_cast;
};

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
#include "arecibo/receptor/detail/UdpStreamTraits.cpp"

#endif // ARECIBO_ALFABURST_RECEPTOR_UDPSTREAMTRAITS_H 
