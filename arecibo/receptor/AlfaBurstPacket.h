#ifndef ARECIBO_ALFABURST_ALFABURSTPACKET_H
#define ARECIBO_ALFABURST_ALFABURSTPACKET_H

#include <cheetah/data/Units.h>
#include <cstdlib>
#include <stdint.h>
#include <stdio.h>
#include <netinet/in.h>

namespace arecibo {
namespace alfaburst {

/**
 * @brief
 *    Description of the UDP stream packets for alfaburst 
 */
class AlfaBurstPacket
{
        static const std::size_t _payload_size = 8192;

    public:
        struct Sample {
            public:
                Sample();
                Sample(unsigned short, unsigned short, unsigned short, unsigned short);
                inline unsigned short xx() const { return ntohs(_xx); }
                inline unsigned short yy() const { return ntohs(_yy); }
                inline unsigned short xy_re() const { return ntohs(_xy_re); }
                inline unsigned short xy_im() const { return ntohs(_xy_im); }

            private:
                unsigned short _xx;
                unsigned short _yy;
                unsigned short _xy_re;
                unsigned short _xy_im;
        };

        struct Footer {
            char footer[8];
        };

    public:
        AlfaBurstPacket();
        ~AlfaBurstPacket();

        /**
         * @brief the total size of the udp packets header
         */
        static std::size_t header_size();

        /**
         * @brief the total size of the udp packets footer
         */
        static std::size_t footer_size();

        /**
         * @brief the footer object
         */
        Footer const& footer() const;

        /**
         * @brief the total size in bytes of the channel data
         */
        static std::size_t payload_size();

        /**
         * @brief the total number of samples in the data payload 
         */
        static std::size_t number_of_samples();

        /**
         * @brief the total number of frequencey channels in the data payload 
         */
        static std::size_t number_of_channels();

        /**
         * @brief the total size of the packet (header + payload + footer)
         */
        static std::size_t size();

        /**
         * @brief start of the channel sample data 
         */
        char* data();

        /**
         * @brief set the counter in the header
         * @details this is stored as a 48bit unsigned integer
         *          anything larger than this passed will have the 48bit modulo applied modulo
         *           n.b range is only up to 48bits
         */
        void set_packet_count(uint64_t);

        /**
         * @brief get the counter info from header
         */
        uint64_t packet_count() const;

        /**
         * @brief set the beam number
         */
        void set_beam(uint8_t beam_number);
        
        /**
         * @return the beam number
         */
        uint8_t beam() const;

        /**
         * @brief set the spectral_quarter number
         * @param spectral_quarter values 0,1,2,3 only
         */
        void set_spectral_quarter(uint8_t spectral_quarter);
        
        /**
         * @return the spectral_quarter number
         */
        uint8_t spectral_quarter() const;

        /**
         * @brief The bandwidth represented by each channel
         */
        static const boost::units::quantity<ska::cheetah::data::MegaHertz, double> channel_bandwidth();

        /**
         * @brief insert a sample
         */
        void insert(std::size_t sample_number, Sample s);
        
        /**
         * @brief return the sampke data
         */
        Sample const& sample(std::size_t sample) const;

        const Sample* begin() const;
        const Sample* end() const;

    private:
        struct Header {
            uint8_t _counter[6]; // integer counter 6 bytes
            uint8_t _spec_quart = 0;
            uint8_t _beam;
        } _header;

        Sample _data[_payload_size/sizeof(Sample)];

        Footer _footer; // footer
};

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_ALFABURSTPACKET_H 
