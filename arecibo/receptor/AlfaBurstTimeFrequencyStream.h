#ifndef ARECIBO_ALFABURST_ALFABURSTTIMEFREQUENCYSTREAM_H
#define ARECIBO_ALFABURST_ALFABURSTTIMEFREQUENCYSTREAM_H


#include "arecibo/receptor/UdpStreamTraits.h"
#include "arecibo/receptor/Config.h"
#include "arecibo/utils/Redis.h"
#include "cheetah/utils/ModifiedJulianClock.h"
#include "panda/PacketStream.h"
#include "panda/ConnectionTraits.h"

namespace arecibo {
namespace alfaburst {
namespace receptor {
class AlfaBurstConfiguration;

/**
 * @class AlfaBurstTimeFrequencyStream
 * @brief
 *    Reads UDP time and frequency data in to a TimeFrequency dat chunk
 *
 * @details
 *
 */
template<typename Producer, typename TimeFrequencyNumericType>
class AlfaBurstTimeFrequencyStream : public ska::panda::PacketStream<AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>, ska::panda::ConnectionTraits<ska::panda::Udp>, UdpStreamTraits<TimeFrequencyNumericType>, Producer>
{
        typedef UdpStreamTraits<TimeFrequencyNumericType> DataTraits;
        typedef ska::panda::ConnectionTraits<ska::panda::Udp> ConnectionTraits;
        typedef ska::panda::PacketStream<AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>, ConnectionTraits, DataTraits, Producer> BaseT;
        typedef typename BaseT::LimitType LimitType;

    public:
        typedef boost::units::quantity<ska::cheetah::data::MegaHertz, double> FrequencyType;

    public:
        AlfaBurstTimeFrequencyStream(Config const& config);
        ~AlfaBurstTimeFrequencyStream();

        void init() override;

        template<typename DataType>
        std::shared_ptr<DataType> get_chunk(LimitType sequence_number, AlfaBurstPacket const&);

        template<typename DataType>
        std::shared_ptr<DataType> get_chunk(DataType const& previous);

    private:
        void get_fpga_reset_time();
        void get_fch1();

    private:
        typedef typename ska::cheetah::data::TimeFrequency<Cpu, TimeFrequencyNumericType>::TimeType TimeType;

    private:
        receptor::Config const& _config;
        typename ska::cheetah::utils::ModifiedJulianClock::time_point _fpga_reset_time;
        double _tsamp; // milliseconds
        FrequencyType _fch1;
        static FrequencyType _foff;
        Redis _db;
};

/**
 *  @brief  use this class to construct a type that takes a single Producer parameter
 *  when you know the data type
 *  @code
 *     template<template<typename> class T>
 *     strict MyClass {
 *     };
 *
 *     MyClass<BindAlfaBurstTimeFrequencyStream<uint_8>::template Type>
 *  @endcode
 */
template<class TimeFrequencyNumericType>
struct BindAlfaBurstTimeFrequencyStream
{
    template<typename T> using Type = AlfaBurstTimeFrequencyStream<T, TimeFrequencyNumericType>;
};

/**
 * @ brief a standalone AlfaBurstTimeFrequencyStream Producer
 */
template<typename NumericType>
class AlfaBurstTimeFrequencyStreamProducer : public AlfaBurstTimeFrequencyStream<AlfaBurstTimeFrequencyStreamProducer<NumericType>, NumericType>
{
        typedef AlfaBurstTimeFrequencyStream<AlfaBurstTimeFrequencyStreamProducer<NumericType>, NumericType> BaseT;

    public:
        template<typename... Args>
        AlfaBurstTimeFrequencyStreamProducer(Args&&... args)
            : BaseT(std::forward<Args>(args)...)
        {
        }
};

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
#include "arecibo/receptor/detail/AlfaBurstTimeFrequencyStream.cpp"

#endif // ARECIBO_ALFABURST_ALFABURSTTIMEFREQUENCYSTREAM_H
