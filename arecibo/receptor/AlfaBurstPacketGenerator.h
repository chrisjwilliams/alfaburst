#ifndef ARECIBO_ALFABURST_RECEPTOR_ALFABURSTPACKETGENERATOR_H
#define ARECIBO_ALFABURST_RECEPTOR_ALFABURSTPACKETGENERATOR_H

#include "AlfaBurstPacket.h"
#include "cheetah/generators/TimeFrequencyGenerator.h"
#include "cheetah/data/TimeFrequency.h"
#include "panda/PacketStream.h"

namespace arecibo {
namespace alfaburst {
namespace receptor {

/**
 * @brief
 *    Generates a stream of Packets with data from the provided data model
 * @details
 * 
 */
class AlfaBurstPacketGenerator
{
        typedef ska::cheetah::data::TimeFrequency<ska::cheetah::Cpu, uint16_t> DataType;
        typedef ska::cheetah::generators::TimeFrequencyGenerator<uint16_t> DataGenerator;

    public:
        AlfaBurstPacketGenerator(DataGenerator& model, std::size_t number_of_channels);
        ~AlfaBurstPacketGenerator();

        ska::panda::Buffer<char> const& next();
        long interval() const;

    private:
        typedef AlfaBurstPacket Packet;
        typedef typename std::result_of<decltype(&Packet::packet_count)(Packet)>::type CounterType;
        
        DataGenerator& _model;             // The data generation model
        DataType _data;
        typename DataType::ConstIterator _data_iterator;
        CounterType _counter;              // (6-Byte) packet counter.
        char _spec_quart;                  // Spectral quarter (0 <= _specQuart < 4)
        char _beam;                        // Beam number (0 <= _beam < 8)
        long _interval;                    // The interval between packets in microsec.

        const std::size_t _max_buffers;
        std::size_t _buffer_index;
        std::vector<ska::panda::Buffer<char>> _buffers;
        ska::panda::Engine _engine;

};

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_RECEPTOR_ALFABURSTPACKETGENERATOR_H 
