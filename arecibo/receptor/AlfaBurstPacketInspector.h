#ifndef ARECIBO_ALFABURST_ALFABURSTPACKETINSPECTOR_H
#define ARECIBO_ALFABURST_ALFABURSTPACKETINSPECTOR_H

#include "AlfaBurstPacket.h"


namespace arecibo {
namespace alfaburst {

/**
 * @brief
 *    Inspector for analysing in packet data to adapt to a panda::PacketStream
 *
 * @details
 * 
 */
class AlfaBurstPacketInspector
{
    public:
        typedef AlfaBurstPacket Packet;

    public:
        AlfaBurstPacketInspector(Packet const&);
        ~AlfaBurstPacketInspector();

        operator Packet const&() const { return _packet; };

        // should the packet be ignored?
        bool ignore() { return false; } // every packet is important to us

    private:
        Packet const& _packet;
};

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_ALFABURSTPACKETINSPECTOR_H 
