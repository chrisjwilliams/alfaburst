#ifndef ARECIBO_ALFABURST_RECEPTOR_CONFIG_H
#define ARECIBO_ALFABURST_RECEPTOR_CONFIG_H


#include "SampleCastConfig.h"
#include "arecibo/utils/RedisConfig.h"
#include "pss/astrotypes/units/Units.h"
#include <panda/ConfigModule.h>
#include <panda/ProcessingEngine.h>
#include <panda/ProcessingEngineConfig.h>
#include <panda/IpAddress.h>
#include <panda/EndpointConfig.h>
#include <boost/asio.hpp>
#include <memory>

namespace arecibo {
namespace alfaburst {
namespace receptor {

/**
 * @brief
 * 
 * @details
 * 
 */
class Config : public ska::panda::ConfigModule
{
    public:
        Config();
        ~Config();

        ska::panda::ProcessingEngine& engine() const;

        /// Set the number of threads to dedicate to the recptro
        void number_of_threads(unsigned threads);

        boost::asio::ip::udp::endpoint datastream_end_point() const;
        void datastream_end_point(boost::asio::ip::udp::endpoint const& endpoint);

        /**
         * @brief get/set the number of time samples to use in each chunk
         */
        std::size_t number_of_time_samples() const;
        void number_of_time_samples(std::size_t);

        template<typename T>
        SampleCastConfig const& sample_cast() const;

        template<typename T>
        SampleCastConfig& sample_cast();

        unsigned packets_per_spectrum() const;
        std::size_t chunk_size() const;

        /**
         * @brief set redis server
         */
        RedisConfig const& redis_config() const;
        void redis_config(RedisConfig const&);

        /**
         * @brief the time sample interval
         */
        boost::units::quantity<pss::astrotypes::units::MilliSecond, double> sample_interval() const;

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        std::size_t _number_of_samples;
        boost::units::quantity<pss::astrotypes::units::MilliSecond, double> _sample_interval;
        mutable std::unique_ptr<ska::panda::ProcessingEngine> _engine;
        ska::panda::EndpointConfig _endpoint_config;
        SampleCastConfig _8bit_sample_config;
        SampleCastConfig _16bit_sample_config; // empty dummy - not actually configurable
        RedisConfig _redis_config;
        ska::panda::ProcessingEngineConfig _engine_config;
};

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_RECEPTOR_CONFIG_H 
