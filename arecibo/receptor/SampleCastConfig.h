#ifndef ARECIBO_ALFABURST_RECEPTOR_SAMPLECASTCONFIG_H
#define ARECIBO_ALFABURST_RECEPTOR_SAMPLECASTCONFIG_H

#include "panda/ConfigModule.h"

namespace arecibo {
namespace alfaburst {
namespace receptor {

/**
 * @brief
 *    Configuration options for inline data conversion of UDP packet data
 *
 * @details
 * 
 */
class SampleCastConfig : public ska::panda::ConfigModule
{
        typedef ska::panda::ConfigModule BaseT;

    public:
        SampleCastConfig();
        ~SampleCastConfig();

        /**
         * @brief the specified cutoff value (max value to be rescaled to the new type)
         */
        uint16_t cutoff() const;
        void cutoff(uint16_t);

    protected:
        void add_options(OptionsDescriptionEasyInit&) override;

    private:
        uint16_t _cutoff;
};

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_RECEPTOR_SAMPLECASTCONFIG_H 
