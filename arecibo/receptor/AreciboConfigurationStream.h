#ifndef ARECIBO_ALFABURST_ARECIBOCONFIGURATIONSTREAM_H
#define ARECIBO_ALFABURST_ARECIBOCONFIGURATIONSTREAM_H


#include "arecibo/data/AreciboConfigurationData.h"
#include <panda/Producer.h>

namespace arecibo {
namespace alfaburst {

/**
 * @brief
 * 
 * @details
 * 
 */
template<typename Producer>
class AreciboConfigurationStream : public ska::panda::Producer<Producer, ska::panda::ServiceData<data::AreciboConfigurationData>>
{
    public:
        AreciboConfigurationStream();
        ~AreciboConfigurationStream();

        void init() override;

    private:
};

} // namespace alfaburst
} // namespace arecibo
#include "arecibo/receptor/detail/AreciboConfigurationStream.cpp"

#endif // ARECIBO_ALFABURST_ARECIBOCONFIGURATIONSTREAM_H 
