#include "arecibo/receptor/AreciboConfigurationStream.h"

namespace arecibo {
namespace alfaburst {


template<typename Producer>
AreciboConfigurationStream<Producer>::AreciboConfigurationStream()
{
}

template<typename Producer>
AreciboConfigurationStream<Producer>::~AreciboConfigurationStream()
{
}

template<typename Producer>
void AreciboConfigurationStream<Producer>::init()
{
    // temp just get a first chunk and allow it to go out of scope immediately so it propagates into the pipeline
    this->template get_chunk<data::AreciboConfigurationData>();
}

} // namespace alfaburst
} // namespace arecibo
