#include "arecibo/receptor/UdpStreamTraits.h"
#include "cheetah/data/TimeFrequency.h"
#include "panda/Log.h"
#include <stdio.h>
#include <algorithm>

namespace arecibo {
namespace alfaburst {
namespace receptor {

constexpr uint64_t UdpStreamTraitsBase::max_sequence_number()
{
    return 0x0000ffffffffffff * 4; // packet number is reused for each spectral quater
}

template<typename TimeFrequencyNumericType>
UdpStreamTraits<TimeFrequencyNumericType>::UdpStreamTraits(SampleCastConfig const& config) 
    : _sample_cast(config) 
{
}

template<typename TimeFrequencyNumericType>
template<typename ContextType>
void UdpStreamTraits<TimeFrequencyNumericType>::deserialise_packet(ContextType& context, PacketInspector const& packet_inspector)
{
    auto const& packet = static_cast<PacketInspector::Packet const&>(packet_inspector);

    auto& chunk = context.chunk();
    typename DataType::Iterator it=chunk.begin() + context.offset();
    const AlfaBurstPacket::Sample* sample = packet.begin() + context.packet_offset();
    const AlfaBurstPacket::Sample* last_sample = sample + context.size();;
    while(sample != last_sample) {
        assert(it < chunk.end());
        *it = _sample_cast(sample->xx() + sample->yy());
        ++it;
        ++sample;
    }
}

template<typename TimeFrequencyNumericType>
template<typename ContextType>
void UdpStreamTraits<TimeFrequencyNumericType>::resize_chunk(ContextType& context)
{
    PANDA_LOG << "resizing chunk:" << context;
    context.chunk().resize(ska::cheetah::data::DimensionSize<ska::cheetah::data::Time>(context.size()/context.chunk().number_of_channels()));
}

template<typename TimeFrequencyNumericType>
unsigned UdpStreamTraits<TimeFrequencyNumericType>::chunk_size(DataType const& chunk)
{
    return chunk.data_size();
}

template<typename TimeFrequencyNumericType>
void UdpStreamTraits<TimeFrequencyNumericType>::set(SampleCastConfig const& config)
{
    _sample_cast.set(config);
}

template<typename ContextType>
void UdpStreamTraitsBase::process_missing_slice(ContextType& context, std::size_t)
{
    PANDA_LOG_WARN << "processing missing packet: " << context << "context_size=" << context.size();

    auto& chunk = context.chunk();
    //typedef typename std::decay<decltype(chunk)>::type::DataType NumericType;
    auto it = chunk.begin() + context.offset();
    auto end=it + context.size();
    while(it!=end) {
        //*it = std::numeric_limits<NumericType>::max();
        *it = 0;
        ++it;
    }
}

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
