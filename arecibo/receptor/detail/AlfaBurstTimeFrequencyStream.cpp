#include "arecibo/receptor/AlfaBurstTimeFrequencyStream.h"
#include "arecibo/receptor/Config.h"
#include "arecibo/utils/RedisReply.h"
#include "panda/Error.h"


namespace arecibo {
namespace alfaburst {
namespace receptor {

// --- setup static vars
// fixed channel bandwidth

template<typename Producer, typename TimeFrequencyNumericType>
typename AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>::FrequencyType AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>::_foff(-1.0*AlfaBurstPacket::channel_bandwidth());


template<typename Producer, typename TimeFrequencyNumericType>
AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>::AlfaBurstTimeFrequencyStream(Config const& config)
try : BaseT( DataTraits(config.sample_cast<TimeFrequencyNumericType>()), config.engine(), ConnectionTraits::SocketType(config.engine(), config.datastream_end_point()))
    , _config(config)
    , _tsamp(config.sample_interval().value())
    , _db(config.redis_config())
{
    get_fpga_reset_time();
    get_fch1();
    this->purge_cadence(300);
    this->missing_packets_before_realign(12000);
}
catch(boost::system::system_error const& e) {
    ska::panda::Error addressed_error(e.what());
    addressed_error << "(" << config.datastream_end_point().address().to_string()
                    << ":" << config.datastream_end_point().port() << ")";
    throw addressed_error;
}

template<typename Producer, typename TimeFrequencyNumericType>
AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>::~AlfaBurstTimeFrequencyStream()
{
}

template<typename Producer, typename TimeFrequencyNumericType>
void AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>::init()
{
    this->BaseT::init();
    this->start();
}

template<typename Producer, typename TimeFrequencyNumericType>
template<typename DataType>
std::shared_ptr<DataType> AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>::get_chunk(LimitType sequence_number, AlfaBurstPacket const&)
{
    auto chunk = this->BaseT::template get_chunk<DataType>();
    if (chunk->number_of_spectra() == 0) {
        chunk->resize( ska::cheetah::data::DimensionSize<ska::cheetah::data::Time>(_config.number_of_time_samples())
                     , ska::cheetah::data::DimensionSize<ska::cheetah::data::Frequency>(4 * DataTraits::channels_per_quarter()));
    }
    chunk->start_time(_fpga_reset_time  + std::chrono::duration<double, std::milli>(_tsamp * (sequence_number/4)));
    chunk->sample_interval(TimeType(_tsamp * ska::cheetah::data::milliseconds));
    chunk->set_channel_frequencies_const_width(_fch1, _foff);
    return chunk;
}

template<typename Producer, typename TimeFrequencyNumericType>
template<typename DataType>
std::shared_ptr<DataType> AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>::get_chunk(DataType const& previous)
{
    auto chunk = this->BaseT::template get_chunk<DataType>(
        ska::cheetah::data::DimensionSize<ska::cheetah::data::Time>(previous.number_of_spectra())
       ,ska::cheetah::data::DimensionSize<ska::cheetah::data::Frequency>(previous.number_of_channels())
    );
    chunk->start_time(previous.end_time());
    chunk->sample_interval(previous.sample_interval());
    chunk->set_channel_frequencies_const_width(_fch1, _foff);
    return chunk;
}

template<typename Producer, typename TimeFrequencyNumericType>
void AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>::get_fch1()
{
    // only 300MHz available drawin from the 448MHz range allowed for by the FPGA
    static const FrequencyType half_bandwidth((448.0/4096.0) * boost::units::si::mega * ska::cheetah::data::hertz *2048.0);
    static const FrequencyType default_central(1599.0 * boost::units::si::mega * ska::cheetah::data::hertz);
    static const FrequencyType default_fch1(default_central + half_bandwidth);

    try {
        RedisReply reply = _db.query("HGET SCRAM:IF1 IF1RFFRQ"); // this is the center frequency in Hz
        if (reply.is_error())
        {
            _fch1 = default_fch1;
            PANDA_LOG_ERROR  << "ERROR: Getting center frequency from redis " << reply.str() << " assuming value " << default_central;
            return;
        }
        const FrequencyType freq_centre(std::stof(reply.str()) * ska::cheetah::data::hertz);
        _fch1 = freq_centre + half_bandwidth;
    } catch(std::exception const& e) {
        _fch1 = default_fch1;
        PANDA_LOG_ERROR  << "ERROR: Getting center frequency from redis " << e.what() << " assuming value " << default_central;
        return;
    }
    PANDA_LOG << "frequency first channel(fch1) = " << _fch1;
    PANDA_LOG << "channel offset= " << _foff;
}

template<typename Producer, typename TimeFrequencyNumericType>
void AlfaBurstTimeFrequencyStream<Producer, TimeFrequencyNumericType>::get_fpga_reset_time()
{
    ska::cheetah::utils::ModifiedJulianClock::time_point now = ska::cheetah::utils::ModifiedJulianClock::now();
    try {
        RedisReply reply = _db.query("GET s6_mcount_0");
        if (reply.is_error())
        {
            _fpga_reset_time = now;
            PANDA_LOG_ERROR << "ERROR: Getting MJD from redis failed! Start MJD will be current time :" << now << " error: \"" << reply.str() << "\"";
            return;
        }
        // mcount0time is the number of seconds elapsed since the Epoch,
        // 1970-01-01 00:00:00 +0000 (UTC), when the mcount on the ROACH2s were
        // reset to 0
        auto unix_time = std::chrono::system_clock::time_point(std::chrono::seconds(std::stol(reply.str())));
        _fpga_reset_time = static_cast<ska::cheetah::utils::ModifiedJulianClock::time_point>(unix_time);
    } catch(std::exception const& e) {
        _fpga_reset_time = now;
        PANDA_LOG_ERROR << "ERROR: Getting MJD from redis failed! Start MJD will be current time :" << now << " error: \"" << e.what() << "\"";
        return;
    }

    PANDA_LOG << "fpga reset time from redis: " << _fpga_reset_time;
    ska::cheetah::utils::ModifiedJulianClock::duration span = std::chrono::duration<float, std::milli>(_tsamp * (DataTraits::max_sequence_number()/4));
    if(_fpga_reset_time + span  < now) // max range we can calculate has been exceeded so adjust appropriately
    {
        _fpga_reset_time += span * (unsigned)((now - _fpga_reset_time)/span);
    }
    PANDA_LOG << "calculated epoch time (MJD): " << _fpga_reset_time;
}

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
