#include "arecibo/receptor/AlfaBurstStream.h"


namespace arecibo {
namespace alfaburst {
namespace receptor {

template<class TimeFrequencyNumericType>
AlfaBurstStream<TimeFrequencyNumericType>::AlfaBurstStream(Config const& config)
    : BaseT(new AlfaBurstTimeFrequencyStream<SelfType, TimeFrequencyNumericType>(config), new AreciboConfigurationStream<AlfaBurstStream>())
{
}

template<class TimeFrequencyNumericType>
AlfaBurstStream<TimeFrequencyNumericType>::~AlfaBurstStream()
{
}

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
