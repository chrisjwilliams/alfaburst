#ifndef ARECIBO_ALFABURST_SAMPLECAST_H
#define ARECIBO_ALFABURST_SAMPLECAST_H

#include "../SampleCastConfig.h"


namespace arecibo {
namespace alfaburst {
namespace receptor {

/**
 * @brief
 *    Conversion of 16bit to 8bit type
 *
 * @details
 * 
 */
template<typename NumericalT>
class SampleCast
{
    public:
        SampleCast(SampleCastConfig const&);
        ~SampleCast();

        void set(SampleCastConfig const&);

        NumericalT operator()(uint16_t) const;

    private:
        uint16_t _cutoff;
};

template<>
class SampleCast<uint16_t>
{
    public:
        SampleCast(SampleCastConfig const& = SampleCastConfig()) {};
        inline uint16_t operator()(uint16_t val) const { return val; }
        void set(SampleCastConfig const&) {};
};


} // namespace receptor
} // namespace alfaburst
} // namespace arecibo

#include "SampleCast.cpp"

#endif // ARECIBO_ALFABURST_SAMPLECAST_H 
