#include "arecibo/receptor/detail/SampleCast.h"


namespace arecibo {
namespace alfaburst {
namespace receptor {


template<typename NumericalT>
SampleCast<NumericalT>::SampleCast(SampleCastConfig const& config)
{
    set(config);
}

template<typename NumericalT>
SampleCast<NumericalT>::~SampleCast()
{
}

template<typename NumericalT>
NumericalT SampleCast<NumericalT>::operator()(uint16_t val) const
{
    // max value cutoff 
    if(val>_cutoff) val=_cutoff;

    static const double ratio = _cutoff / std::numeric_limits<NumericalT>::max();
    return static_cast<NumericalT>((double)val / ratio);
}

template<typename NumericalT>
void SampleCast<NumericalT>::set(SampleCastConfig const& config)
{
    _cutoff = config.cutoff();
    PANDA_LOG << "sample cutoff=" << _cutoff;
}

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
