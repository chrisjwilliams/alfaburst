#ifndef ARECIBO_ALFABURST_ALFABURSTSTREAM_H
#define ARECIBO_ALFABURST_ALFABURSTSTREAM_H


#include "arecibo/receptor/AlfaBurstTimeFrequencyStream.h"
#include "arecibo/receptor/AreciboConfigurationStream.h"
#include "panda/ProducerAggregator.h"

namespace arecibo {
namespace alfaburst {
namespace receptor {
class Config;

/**
 * @class AlfaBurstStream
 * @brief
 *    The main Producer for UDP stream data and database config
 *
 * @details
 * @param TimeFrequencyNumericType the numerical representation of the channel vlaues in the TimeFrequency class
 */
//#define ALFABURSTSTREAM_PARAM_PACK AlfaBurstStream<TimeFrequencyNumericType>, AlfaBurstTimeFrequencyStreamGen<TimeFrequencyNumericType>::template Type, AreciboConfigurationStream
#define ALFABURSTSTREAM_PARAM_PACK AlfaBurstStream<TimeFrequencyNumericType>, BindAlfaBurstTimeFrequencyStream<TimeFrequencyNumericType>::template Type, AreciboConfigurationStream
template<class TimeFrequencyNumericType>
class AlfaBurstStream : public ska::panda::ProducerAggregator<ALFABURSTSTREAM_PARAM_PACK>
{
        typedef AlfaBurstStream<TimeFrequencyNumericType> SelfType;
        typedef ska::panda::ProducerAggregator<ALFABURSTSTREAM_PARAM_PACK> BaseT;

    public:
        AlfaBurstStream(Config const& config);
        ~AlfaBurstStream();

    private:
};

#undef ALFABURSTSTREAM_PARAM_PACK
} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
#include "detail/AlfaBurstStream.cpp"

#endif // ARECIBO_ALFABURST_ALFABURSTSTREAM_H 
