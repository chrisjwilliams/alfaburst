#include "arecibo/receptor/AlfaBurstPacketInspector.h"


namespace arecibo {
namespace alfaburst {

AlfaBurstPacketInspector::AlfaBurstPacketInspector(Packet const& packet)
    : _packet(packet)
{
}

AlfaBurstPacketInspector::~AlfaBurstPacketInspector()
{
}

} // namespace alfaburst
} // namespace arecibo
