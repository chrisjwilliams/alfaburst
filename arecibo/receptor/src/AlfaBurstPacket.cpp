#include "arecibo/receptor/AlfaBurstPacket.h"
#include "panda/Log.h"
#include <cassert>


namespace arecibo {
namespace alfaburst {

AlfaBurstPacket::Sample::Sample()
{
}

AlfaBurstPacket::Sample::Sample(unsigned short xx, unsigned short yy, unsigned short xy_re, unsigned short xy_im)
    : _xx(htons(xx))
    , _yy(htons(yy))
    , _xy_re(htons(xy_re))
    , _xy_im(htons(xy_im))
{
}

AlfaBurstPacket::AlfaBurstPacket()
{
}

AlfaBurstPacket::~AlfaBurstPacket()
{
}

std::size_t AlfaBurstPacket::header_size()
{
    return sizeof(Header);
}

const boost::units::quantity<ska::cheetah::data::MegaHertz, double> AlfaBurstPacket::channel_bandwidth()
{
    // Reference: 
    // SETIBURST: A Robotic, Commensal, Realtime Multi-Science Backend for the Arecibo Telescope
    // Jayanth Chennamangalam et al
    // arXiv:1701.04538
    static const boost::units::quantity<ska::cheetah::data::MegaHertz, double>
                    channel_bandwidth((224.0/4096.0) * boost::units::si::mega * ska::cheetah::data::hertz);
    return channel_bandwidth;
}

std::size_t AlfaBurstPacket::footer_size()
{
    return sizeof(Footer);
}

std::size_t AlfaBurstPacket::payload_size()
{
    return _payload_size;
}

std::size_t AlfaBurstPacket::size()
{
    return _payload_size + footer_size() + header_size();
}

std::size_t AlfaBurstPacket::number_of_samples() //n.b NOT time samples
{
    return _payload_size/sizeof(Sample);
}

std::size_t AlfaBurstPacket::number_of_channels()
{
    return number_of_samples();
}

void AlfaBurstPacket::set_beam(uint8_t beam_number)
{
    _header._beam = beam_number;
}

uint8_t AlfaBurstPacket::beam() const
{
    return _header._beam;
}

void AlfaBurstPacket::set_spectral_quarter(uint8_t quarter)
{
    assert(quarter < 4);
    _header._spec_quart=quarter;
}

uint8_t AlfaBurstPacket::spectral_quarter() const
{
    return _header._spec_quart;
}

void AlfaBurstPacket::set_packet_count(uint64_t packet_count)
{
    packet_count%=(0x1000000000000);
    //*(((unsigned int *) _header._counter)) = (uint64_t) (packet_count & 0x000000FFFFFFFFFF);
    //*(((unsigned short int *) _header._counter) + 2) = (unsigned unsigned short int) ((packet_count & 0x0000FFFF00000000) >> 32);
    _header._counter[0] = static_cast<uint8_t>(packet_count >> 40);
    _header._counter[1] = static_cast<uint8_t>(packet_count >> 32);
    _header._counter[2] = static_cast<uint8_t>(packet_count >> 24);
    _header._counter[3] = static_cast<uint8_t>(packet_count >> 16);
    _header._counter[4] = static_cast<uint8_t>(packet_count >> 8);
    _header._counter[5] = static_cast<uint8_t>(packet_count);
}

uint64_t AlfaBurstPacket::packet_count() const
{
    uint64_t counter = (*((uint64_t *) _header._counter))
                                        & 0x0000FFFFFFFFFFFF;
    return (unsigned long)
                      (((counter & 0x0000FF0000000000) >> 40)
                     + ((counter & 0x000000FF00000000) >> 24)
                     + ((counter & 0x00000000FF000000) >> 8)
                     + ((counter & 0x0000000000FF0000) << 8)
                     + ((counter & 0x000000000000FF00) << 24)
                     + ((counter & 0x00000000000000FF) << 40));
}

void AlfaBurstPacket::insert(std::size_t sample_number, Sample s)
{
    assert(sample_number < _payload_size/sizeof(Sample));
    //std::swap(_data[sample_number], s);
    _data[sample_number] =  s;
}

AlfaBurstPacket::Sample const& AlfaBurstPacket::sample(std::size_t sample_number) const
{
    return _data[sample_number];
}

AlfaBurstPacket::Footer const& AlfaBurstPacket::footer() const
{
    return _footer;
}

const typename AlfaBurstPacket::Sample* AlfaBurstPacket::begin() const
{
    return &_data[0];
}

const typename AlfaBurstPacket::Sample* AlfaBurstPacket::end() const
{
    return &_data[_payload_size/sizeof(Sample)];
}

} // namespace alfaburst
} // namespace arecibo
