#include "arecibo/receptor/Config.h"
#include "panda/ProcessingEngineConfig.h"


namespace arecibo {
namespace alfaburst {
namespace receptor {


Config::Config()
    : ska::panda::ConfigModule("udp")
    , _sample_interval(0.128 * pss::astrotypes::units::milliseconds)
    , _endpoint_config("listen")
{
    _endpoint_config.address(ska::panda::IpAddress(16704, "0.0.0.0"));
    add(_endpoint_config);
    add(_8bit_sample_config);
    add(_redis_config);
//    add(_engine_config);
}

Config::~Config()
{
}

void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
        ("time_samples_chunk", boost::program_options::value<std::size_t>(&_number_of_samples)->default_value(128), "the number of time samples in each chunk")
        ("sample_interval", boost::program_options::value<double>()->default_value(_sample_interval.value())->notifier(
           [this](double ms) {
                _sample_interval=ms * pss::astrotypes::units::milliseconds;
           }
        ), "the number of time samples in each chunk (in milliseconds)")
        ("threads", boost::program_options::value<unsigned>()->default_value(2U)->notifier(
            [this](unsigned threads) {
                number_of_threads(threads);
            }) , "the number of threads to dedicate to UDP packet handling");
}

void Config::number_of_threads(unsigned threads)
{
    ska::panda::ProcessingEngineConfig config(threads);
    //config.affinities({0,1,6,7});
    //_engine_config.number_of_threads(threads);
    _engine.reset(new ska::panda::ProcessingEngine(config));
}

unsigned Config::packets_per_spectrum() const
{
    return 31;
}

std::size_t Config::chunk_size() const
{
    return std::size_t(1024);
}

ska::panda::ProcessingEngine& Config::engine() const
{
    assert(_engine);
    return *_engine;
}

boost::asio::ip::udp::endpoint Config::datastream_end_point() const
{
    return _endpoint_config.address().end_point<boost::asio::ip::udp::endpoint>();
}

void Config::datastream_end_point(boost::asio::ip::udp::endpoint const& endpoint)
{
    _endpoint_config.address(ska::panda::IpAddress(endpoint));
}

std::size_t Config::number_of_time_samples() const
{
    return _number_of_samples;
}

void Config::number_of_time_samples(std::size_t n)
{
    _number_of_samples = n;
}

template<>
SampleCastConfig const& Config::sample_cast<uint8_t>() const
{
    return _8bit_sample_config;
}

template<>
SampleCastConfig& Config::sample_cast<uint8_t>()
{
    return _8bit_sample_config;
}

template<>
SampleCastConfig const& Config::sample_cast<uint16_t>() const
{
    return _16bit_sample_config;
}

RedisConfig const& Config::redis_config() const
{
    return _redis_config;
}

void Config::redis_config(RedisConfig const& redis_config)
{
    _redis_config.address(redis_config.address());
}

boost::units::quantity<pss::astrotypes::units::MilliSecond, double> Config::sample_interval() const
{
    return _sample_interval;
}

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
