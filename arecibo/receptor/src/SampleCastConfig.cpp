#include "arecibo/receptor/SampleCastConfig.h"


namespace arecibo {
namespace alfaburst {
namespace receptor {


SampleCastConfig::SampleCastConfig()
    : BaseT("sample_cast")
    , _cutoff(25000)
{
}

SampleCastConfig::~SampleCastConfig()
{
}

void SampleCastConfig::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("cutoff", boost::program_options::value<uint16_t>(&_cutoff)->default_value(_cutoff), "all values above the cutoff value will be set = cutoff");
}

uint16_t SampleCastConfig::cutoff() const
{
    return _cutoff;
}

void SampleCastConfig::cutoff(uint16_t co)
{
    _cutoff = co;
}

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
