#include "arecibo/receptor/UdpStreamTraits.h"


namespace arecibo {
namespace alfaburst {
namespace receptor {


UdpStreamTraitsBase::UdpStreamTraitsBase()
{
}

UdpStreamTraitsBase::~UdpStreamTraitsBase()
{
}

bool UdpStreamTraitsBase::align_packet(PacketInspector const& packet) const
{
    if(static_cast<PacketInspector::Packet const&>(packet).spectral_quarter() == 0) return true;
    return false;
}

uint64_t UdpStreamTraitsBase::sequence_number(PacketInspector const& packet)
{
    return static_cast<PacketInspector::Packet const&>(packet).packet_count() * 4
           + static_cast<PacketInspector::Packet const&>(packet).spectral_quarter();
}


unsigned UdpStreamTraitsBase::packet_size()
{
    return PacketInspector::Packet::number_of_samples();
}

unsigned UdpStreamTraitsBase::channels_per_quarter()
{
    return 1024; // fixed
}

void UdpStreamTraitsBase::packet_stats(uint64_t missing, uint64_t expected_packets)
{
    if(missing > 0) {
        PANDA_LOG_WARN << missing << " missing packets this chunk (out of " << expected_packets << ")";
    }
}

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
