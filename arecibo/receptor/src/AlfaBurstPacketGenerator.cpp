#include "arecibo/receptor/AlfaBurstPacketGenerator.h"


namespace arecibo {
namespace alfaburst {
namespace receptor {


AlfaBurstPacketGenerator::AlfaBurstPacketGenerator(DataGenerator& model, std::size_t number_of_channels)
    : _model(model)
    , _data(ska::cheetah::data::DimensionSize<ska::cheetah::data::Time>(1024), 
            ska::cheetah::data::DimensionSize<ska::cheetah::data::Frequency>(number_of_channels)) // 1024 is not important - cpuld be anything
    , _data_iterator(_data.cend())
    , _counter(0)
    , _spec_quart(0)
    , _beam(0)
    , _interval(1)
    , _max_buffers(8)
    , _buffer_index(0)
{
    for(unsigned i=0; i < _max_buffers; ++i) {
        _buffers.emplace_back(ska::panda::Buffer<char>(sizeof(Packet)));
    }
}

AlfaBurstPacketGenerator::~AlfaBurstPacketGenerator()
{
}

ska::panda::Buffer<char> const& AlfaBurstPacketGenerator::next()
{
    ska::panda::Buffer<char>& buffer = _buffers[++_buffer_index%_max_buffers];

    char* ptr = buffer.data();
    auto packet = new(ptr) Packet; // placement new
    assert(buffer.size() == sizeof(Packet));

    // fill the packet header.
    packet->set_packet_count(_counter);
    packet->set_beam(_beam);
    packet->set_spectral_quarter(_spec_quart);

    // Fill the packet data.
    for (unsigned i = 0; i < AlfaBurstPacket::number_of_samples(); ++i) {
        if(_data_iterator == _data.cend())
        {
            // get next chunk of data from the model
            _model.next(_data);
            _data_iterator = _data.cbegin();
        }

        // emulate linear polarisations only
        packet->insert(i, AlfaBurstPacket::Sample( *_data_iterator, 
                                                   0,
                                                   0, 0));
        ++_data_iterator;
    }


    // Increment counters for next time.
    if(++_spec_quart == 4 ) {
        ++_counter;
        _spec_quart = 0;
    }

    return buffer;
}

long AlfaBurstPacketGenerator::interval() const {
    return _interval;
}

} // namespace receptor
} // namespace alfaburst
} // namespace arecibo
