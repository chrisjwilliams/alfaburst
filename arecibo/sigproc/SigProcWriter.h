#ifndef SKA_CHEETAH_SIGPROC_SIGPROCWRITER_H
#define SKA_CHEETAH_SIGPROC_SIGPROCWRITER_H

#include "arecibo/sigproc/SigProcHeader.h"
#include "arecibo/sigproc/WriterConfig.h"
#include "arecibo/data/TimeFrequency.h"
#include <boost/filesystem.hpp>
#include <fstream>
#include <memory>

namespace arecibo {
namespace alfaburst {
namespace sigproc {

/**
 * @brief
 *    Writes data types to a sigproc format file
 * @details
 *    Will automatically create files in the provided directory with timestamps as the filename
 */
class SigProcWriter
{
    public:
        SigProcWriter(WriterConfig const&);
        SigProcWriter(std::string const& directory);
        SigProcWriter(SigProcWriter&&);
        ~SigProcWriter();

        /**
         * @brief writes out the samples in the data to the currently open sigproc file
         * @details if the sigproc file is not open, or the data parameters do not match 
         * that of the currently open sigproc file, then a new sigproc file will be created
         * with a suitable header.
         */
        template<typename Arch>
        SigProcWriter& operator<<(data::TimeFrequency<Arch> const& t);

    private:
        void set_dir(std::string const& path);
        std::string next_file(data::TimeFrequency<Cpu>::TimePointType const&);

    private:
        boost::filesystem::path _dir;
        SigProcHeader _header;
        std::unique_ptr<std::ofstream> _file_stream;
};

template<typename Arch>
std::ostream& operator<<(std::ostream& os, data::TimeFrequency<Arch> const&);

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
#include "arecibo/sigproc/detail/SigProcWriter.cpp"

#endif // SKA_CHEETAH_SIGPROC_SIGPROCWRITER_H 
