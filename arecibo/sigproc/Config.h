#ifndef ARECIBO_ALFABURST_SIGPROC_CONFIG_H
#define ARECIBO_ALFABURST_SIGPROC_CONFIG_H


#include "panda/ConfigModule.h"

namespace arecibo {
namespace alfaburst {
namespace sigproc {

/**
 * @brief
 *   SigProc configuration parameters
 * 
 */
class Config : public ska::panda::ConfigModule
{
    public:
        Config();
        ~Config();

        /**
         * @brief return the sigproc files to be processed as input data
         */
        std::vector<std::string> const& sigproc_files() const;

        /**
         * @brief return the sigproc files to be processed as input data
         */
        void set_sigproc_files(std::vector<std::string> const& sigproc_files);
        void set_sigproc_files(std::string sigproc_file);

        /**
         * @brief return the default nbits to be used when not specified in sigproc file headers
         */
        int nbits() const;

        /**
         * @brief return the number of time samples in a chunk
         */
        unsigned chunk_samples() const;

        /**
         * @brief setthe number of time samples in a chunk
         */
        void set_chunk_samples(unsigned samples);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        std::vector<std::string> _sigproc_files;
        int _nbits;
        unsigned _chunk_samples;
};

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_SIGPROC_CONFIG_H 
