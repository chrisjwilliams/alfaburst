#ifndef SKA_CHEETAH_TEST_UTILS_SIGPROCTESTFILE_H
#define SKA_CHEETAH_TEST_UTILS_SIGPROCTESTFILE_H


namespace ska {
namespace cheetah {
namespace test_utils {

/**
 * @brief
 * 
 * @details
 * 
 */
class SigProcTestFile
{
    public:
        SigProcTestFile();

        ~SigProcTestFile();

    private:
};

} // namespace test_utils
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_TEST_UTILS_SIGPROCTESTFILE_H 
