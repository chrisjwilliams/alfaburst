#include "arecibo/sigproc/SigProcFileStream.h"
#include "arecibo/data/TimeFrequency.h"

#include "panda/Error.h"
#include <boost/filesystem.hpp>
#include <type_traits>
#include <iostream>
#include <new>


namespace arecibo {
namespace alfaburst {
namespace sigproc {

void char2ints (unsigned char c, int *i, int *j);

void char4ints (unsigned char c, int *i, int *j, int *k, int *l);

// @returns the number of samples read

std::size_t read_block(std::istream& input, int nbits, float *block, unsigned long nread);

template<typename Producer>
SigProcFileStream<Producer>::SigProcFileStream(Config const& config)
    : _config(config)
    , _nsamples(0)
    , _filenames(config.sigproc_files())
    , _file_it(_filenames.cbegin())
{
    _header.set_number_of_bits(config.nbits());
}

template<typename Producer>
SigProcFileStream<Producer>::~SigProcFileStream()
{
}

template<typename Producer>
void SigProcFileStream<Producer>::transfer_header_info(data::TimeFrequency<Cpu>& chunk) 
{
    if( chunk.number_of_samples() != 0 ){
        // assume the chunk wants a set number of samples
        chunk.resize( _header.number_of_channels(), chunk.number_of_samples());
    }
    else if(number_of_data_samples() > 0 ) {
        // we take as many samples os defined in the header
        chunk.resize( _header.number_of_channels(),
                      number_of_data_samples()/ _header.number_of_channels());
    }
    else {
        // make a guess at how many samples to take
        if( _header.number_of_bits() > 0 ) {
            chunk.resize( 
                    _header.number_of_channels(),
                    sizeof(float)*8/_header.number_of_bits());
        }
        else {
            throw ska::panda::Error("unable to determine number of sigproc samples required");
        }
    }
    chunk.sample_interval(_header.sample_interval());
    if(chunk.channel_frequencies().size() > 1) {
        chunk.set_channel_frequencies(_header.channel_frequencies().begin(), _header.channel_frequencies().end());
    }
    else {
        chunk.set_channel_frequencies_const_width(static_cast<typename data::TimeFrequency<Cpu>::FrequencyType>(_header.start_frequency())
                                                 ,static_cast<typename data::TimeFrequency<Cpu>::FrequencyType>(_header.channel_width()));
    }
}

template<typename Producer>
std::size_t SigProcFileStream<Producer>::number_of_data_samples() const
{
    if( _nsamples == 0) {
        _nsamples = (boost::filesystem::file_size(*(_file_it -1)) - _header.size() ) * (8/_header.number_of_bits());
    }
    return _nsamples;
}

template<typename Producer>
bool SigProcFileStream<Producer>::process()
{
    if(!_file_stream.is_open() || _file_stream.eof() || _file_stream.peek() == decltype(_file_stream)::traits_type::eof()) {
        if(!next_file()) return true; // mark end of data
    }

    typedef data::TimeFrequency<Cpu> TimeFrequencyType;
    //typedef std::decay<decltype(std::declval<TimeFrequencyType>().data())>::type TimeFrequencyDataType;

    auto chunk = this->template get_chunk<TimeFrequencyType>(_config.chunk_samples(), 0);
    assert(chunk);
    
    transfer_header_info(*chunk);
    chunk->start_time(_start_time);
    auto it = chunk->begin();
    return fill_chunk(*chunk, 0, it);
}

template<typename Producer>
bool SigProcFileStream<Producer>::fill_chunk(data::TimeFrequency<Cpu>& chunk, std::size_t offset, data::TimeFrequency<Cpu>::Iterator& it )
{
    std::size_t data_to_read = chunk.number_of_samples() * chunk.number_of_channels() - offset;
    // of the read_block function. This is not ideal, but fine for the moment.
    
    std::size_t data_read;
    {
        // TODO - read in directly to the iterator instead of expanding to floats and back to the type via read_block
        float* tmp = (float*)malloc(data_to_read * sizeof(float));
        if(!tmp) throw std::bad_alloc();
        
        try {
            data_read = read_block(_file_stream, _header.number_of_bits(), tmp, data_to_read);
        }
        catch(...) {
            free(tmp);
            throw;
        }
        assert(data_read <= data_to_read);

        // Convert float values back to data::TimeFrequencyDataType
        // Note: This will corrupt data from 16 and 32 bit filterbank 
        // files. We should consider disabling the 16 and 32 bit switch
        // cases in read_block so that this does not happen.
        std::copy(tmp,tmp+data_read,it);
        free(tmp);
    }
    
    // This branch handles the case that we do not get a full chunks
    // worth of data from the read (e.g. when we hit EOF)
    std::size_t valid_samples = data_read / chunk.number_of_channels(); //implicit floor.
    _start_time += std::chrono::duration<double>((double)valid_samples * _header.sample_interval().value());
    if(data_read != data_to_read) 
    {
        if( _file_stream.eof()) {
            auto expected_header = _header;
            // try to get missing chunks from the next file
            bool rv=next_file();
            if(!rv || expected_header.start_time() != _start_time || expected_header != _header) {
                // Calculate the number of valid time samples (i.e. full spectra) and 
                // resize the chuck appropriately.   
                std::cout << "resizeing to " << valid_samples << std::endl;
                chunk.resize(chunk.number_of_channels(), valid_samples);
                return !rv;
            }
            else {
                fill_chunk(chunk, offset + data_read, it);
            }
        }
    }

    // Indicate that stream is still producing
    return false;
}

template<typename Producer>
bool SigProcFileStream<Producer>::next_file()
{
    _nsamples = 0U;

    if(_file_it == _filenames.cend()) return false;

    // open file for input
    _file_stream.exceptions();
    _file_stream.clear();
    _file_stream.open(*_file_it, std::ios::binary);

    if(!_file_stream.is_open()) {
        std::cerr << "could not open file '" << *_file_it << "'" << std::endl; // TODO use logging framework
        return false;
    }
    _file_stream.exceptions(std::ifstream::failbit | std::ifstream::badbit);

    
    // parse the header
    try { 
        // try catch block to append filename to any message
        try {
            // process header
            _header.read(_file_stream);
        } catch(ska::panda::Error& e) {
            e << " in file " << *_file_it;
            throw e;
        }
    }
    catch(...) {
        // recover from a bad parse, moving on to the next file
        ++_file_it;
        _file_stream.close();
        throw;
    }
    _start_time = _header.start_time();

    // increment file iterator for next call
    ++_file_it;

    return true;
}


} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
