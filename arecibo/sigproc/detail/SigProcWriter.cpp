#include "arecibo/sigproc/SigProcWriter.h"

namespace arecibo {
namespace alfaburst {
namespace sigproc {

template<typename Arch>
SigProcWriter& SigProcWriter::operator<<(data::TimeFrequency<Arch> const& tf)
{
    typedef data::TimeFrequency<Arch> TimeFrequencyType;
    SigProcHeader h;
    h.set_telescope_id(1); // 1 = arecibo
    h.set_number_of_channels(tf.number_of_channels());
    h.set_number_of_bits(sizeof(typename TimeFrequencyType::DataType)*8);
    h.set_start_time(tf.start_time());
    h.set_sample_interval(tf.sample_interval());
    h.set_number_of_intermediate_frequencies(1);
    
    if( _header != h) { 
        // if the incomming data is incompatible with existing header info
        // we start a new file
        _file_stream->close();
    }
    if(!_file_stream->is_open()) {
        _header = h;
        _file_stream->open(next_file(tf.start_time()), std::ios::binary);
        _header.write(*_file_stream);
    }

    *_file_stream << tf;

    // update the start time of the next expected block
    _header.set_start_time(_header.start_time() + std::chrono::duration<double>(_header.sample_interval().value() * (double)(_header.number_of_channels() * tf.number_of_samples())));
    return *this;
}

template<typename Arch>
std::ostream& operator<<(std::ostream& os, data::TimeFrequency<Arch> const& data) {
    typedef data::TimeFrequency<Arch> TimeFrequencyType;
    auto it = data.begin();
    while(it != data.end()) {
        os.write(reinterpret_cast<const char*>(&(*it)), sizeof(typename TimeFrequencyType::DataType));
        ++it;
    }
    return os;
}

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
