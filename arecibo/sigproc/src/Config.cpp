#include "arecibo/sigproc/Config.h"


namespace arecibo {
namespace alfaburst {
namespace sigproc {

static const int default_nbits=8;

Config::Config()
    : ska::panda::ConfigModule("sigproc")
    , _sigproc_files()
    , _nbits(default_nbits)
    , _chunk_samples(0)
{
}

Config::~Config()
{
}


void Config::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    ("file", boost::program_options::value<std::vector<std::string>>(&_sigproc_files)->multitoken(), "specify the sigproc file(s) to read as input data")
    ("chunk_samples", boost::program_options::value<unsigned>(&_chunk_samples)->default_value(128), "the number of time samples in each chunk")
    ("default-nbits", boost::program_options::value<int>(&_nbits)->default_value(default_nbits), "specify the default number of bits to use when not specified in sigproc header");
}

std::vector<std::string> const& Config::sigproc_files() const
{
    return _sigproc_files;
}

void Config::set_sigproc_files(std::vector<std::string> const& sigproc_files)
{
    _sigproc_files = sigproc_files;
}

void Config::set_sigproc_files(std::string sigproc_file)
{
    _sigproc_files.emplace_back(sigproc_file);
}

int Config::nbits() const
{
    return _nbits;
}

unsigned Config::chunk_samples() const
{
    return _chunk_samples;
}

void Config::set_chunk_samples(unsigned num)
{
    _chunk_samples = num;
}

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
