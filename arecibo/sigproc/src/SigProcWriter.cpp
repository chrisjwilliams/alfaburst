#include "arecibo/sigproc/SigProcWriter.h"
#include <panda/Error.h>
#include <boost/algorithm/string/trim.hpp>

namespace arecibo {
namespace alfaburst {
namespace sigproc {

std::ostream& operator<<(std::ostream& os, data::TimeFrequency<Cpu> const&);

SigProcWriter::SigProcWriter(WriterConfig const& config)
    : _file_stream(new std::ofstream)
{
    set_dir(config.dir());
}

SigProcWriter::SigProcWriter(std::string const& filename)
    : _file_stream(new std::ofstream)
{
    set_dir(filename);
}

SigProcWriter::~SigProcWriter()
{
}

SigProcWriter::SigProcWriter(SigProcWriter&& s)
    : _dir(std::move(s._dir))
    , _header(std::move(s._header))
    , _file_stream(std::move(s._file_stream))
{
}

void SigProcWriter::set_dir(std::string const& path)
{
    _dir=path;
    if(!boost::filesystem::is_directory(_dir)) {
        ska::panda::Error e("SigProcWriter: directory does not exist:");
        e << _dir;
        throw e;
    }
}

std::string SigProcWriter::next_file(data::TimeFrequency<Cpu>::TimePointType const& time)
{
    std::time_t ttp = std::chrono::system_clock::to_time_t(time);
    std::string stem(std::ctime(&ttp));
    boost::algorithm::trim_right(stem);
    boost::filesystem::path file = _dir / boost::filesystem::path(stem);
    if(boost::filesystem::exists(file)) {
        unsigned count = 0;
        do {
            file = _dir / boost::filesystem::path(stem + "_" + std::to_string(++count));
        } while( boost::filesystem::exists(file));
    }
    return file.native();
}

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
