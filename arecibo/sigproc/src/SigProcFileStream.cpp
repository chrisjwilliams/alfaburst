#include "arecibo/sigproc/SigProcFileStream.h"
#include "arecibo/data/TimeFrequency.h"

#include "panda/Error.h"
#include <boost/filesystem.hpp>
#include <type_traits>
#include <iostream>
#include <new>


namespace arecibo {
namespace alfaburst {
namespace sigproc {

#define HI4BITS   240
#define LO4BITS   15
void char2ints (unsigned char c, int *i, int *j)
{
  *i =  c & LO4BITS;
  *j = (c & HI4BITS) >> 4;
}

void char4ints (unsigned char c, int *i, int *j, int *k, int *l)
{
  *i =  c & 3;
  *j = (c & 12) >> 2;
  *k = (c & 48) >> 4;
  *l = (c & 192) >> 6;
}

// @returns the number of samples read
std::size_t read_block(std::istream& input, int nbits, float *block, unsigned long nread)
{
    int j, k, s1, s2, s3, s4;
    std::size_t i;
    short *shortblock=nullptr;
    char *charblock=nullptr;
    std::size_t iread;

    /* decide how to read the data based on the number of bits per sample */
    try{
    switch(nbits) {

        case 1:
            // read n/8 bytes into character block containing n 1-bit pairs
            charblock = (char *) malloc(nread / 8);
            input.read(charblock, nread / 8);
            k = 0;

            // unpack 1-bit pairs into the datablock
            iread = input.gcount();
            for (std::size_t i = 0; i < iread; ++i)
                for (j = 0; j < 8; ++j) {
                    block[k++] = charblock[i] & 1;
                    charblock[i] >>= 1;
                }

            iread = k; //number of samples read in
            free(charblock);
            break;

        case 2: // NOTE: Handles Parkes Hitrun survey data
            // read n/4 bytes into character block containing n 2-bit pairs
            charblock = (char *) malloc(nread / 4);
            input.read(charblock, nread / 4);
            j = 0;

            iread = input.gcount();
            for(i = 0; i < iread; i++) {
                char4ints(charblock[i], &s1, &s2, &s3, &s4);
                block[j++] = (float) s1;
                block[j++] = (float) s2;
                block[j++] = (float) s3;
                block[j++] = (float) s4;
            }

            iread *= 4;        
            free(charblock);
            break;

        case 4:
            // read n/2 bytes into character block containing n 4-bit pairs
            charblock = (char *) malloc(nread / 2);
            input.read(charblock, nread / 2);
            j = 0;

            /* unpack 4-bit pairs into the datablock */
            iread = input.gcount();
            for (i = 0; i < iread; i++) {
                char2ints(charblock[i], &s1, &s2);
                block[j++] = (float) s1;
                block[j++] = (float) s2;
            }

            iread *= 2; /* this is the number of samples read in */ 
            free(charblock);
            break;

        case 8:
            /* read n bytes into character block containing n 1-byte numbers */
            charblock = (char *) malloc(nread);
            input.read(charblock, nread);
            /* copy numbers into datablock */
            iread = input.gcount();
            for (i = 0; i < iread; i++)
                block[i] = (float) charblock[i];

            free(charblock);
            break;

        case 16:
	    //NOTE: should condsider raising warning if this is used, as 
	    //the data::TimeFrequencyDataType is uint_8.
            /* read 2*n bytes into short block containing n 2-byte numbers */
            shortblock = (short *) malloc(2 * nread);
            input.read(reinterpret_cast<char*>(shortblock), 2 * nread);

            /* copy numbers into datablock */
            iread = input.gcount() / 2;
            for (i = 0; i < iread; i++) {
                block[i] = (float) shortblock[i];
            }

            free(shortblock);
            break;

        case 32:
            /* read 4*n bytes into floating block containing n 4-byte numbers */
            input.read(reinterpret_cast<char*>(block), 4 * nread); 
            iread = input.gcount() / 4;
            break;

        default:
            throw ska::panda::Error("SigProcStream : read_block - nbits can only be 4, 8, 16 or 32!");
            iread = 0;
    }
    } catch (...) {
        if(shortblock) free(shortblock);
        if(charblock) free(charblock);
        iread = input.gcount() * 8/nbits;
        if(!input.eof()) {
            throw;
        }
    }

    /* return number of samples read */
    return iread;
}

SigProcReader::SigProcReader(Config const& config) 
    : SigProcFileStream<SigProcReader>(config)
{
}

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
