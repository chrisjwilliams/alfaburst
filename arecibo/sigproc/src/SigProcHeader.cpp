#include "arecibo/sigproc/SigProcHeader.h"
#include "arecibo/data/Units.h"
#include "panda/Error.h"
#include <boost/units/quantity.hpp>

#include <cstring>
#include <string>
#include <iostream>

namespace arecibo {
namespace alfaburst {
namespace sigproc {


// buffer for reading in data to pass to a std::string - TODO use stream buff directly
static char _buffer[80];

// strong typing for the Header labels
struct SigProcLabel : public std::string
{
    SigProcLabel() {}
    SigProcLabel(std::string const& s) : std::string(s) {}
    SigProcLabel(const char * s) : std::string(s) {}
    std::size_t size() const {
        return std::string::size() + sizeof(int32_t);
    }
};

// sigproc format types are preceded by the size
// read
template<typename T>
unsigned get_var(std::istream& stream, T& var) {
    stream.read(reinterpret_cast<char*>(&var), sizeof(T));
    return stream.gcount();
}

template<>
unsigned get_var<SigProcHeader::FrequencyType>(std::istream& stream, SigProcHeader::FrequencyType& var) {
    typedef SigProcHeader::FrequencyType::value_type ValueType;
    stream.read(reinterpret_cast<char*>(const_cast<double*>(&var.value())), sizeof(ValueType));
    return stream.gcount();
}

// write
template<typename T>
unsigned write_var(std::ostream& stream, T const& var) {
    stream.write(reinterpret_cast<const char*>(&var), sizeof(T));
    return sizeof(T);
}


// sigproc format string types are preceded by the string length
// read
unsigned get_var(std::istream& stream, std::string& var) {
    // read in the size of the string
    int32_t size=0;
    stream.read(reinterpret_cast<char*>(&size), sizeof(size));
    auto s = sizeof(int32_t);

    if( size < 0 || size > 80 ) {
        ska::panda::Error e("SigProc::read: illegal size of string: ");
        e << size;
        throw e;
    }
    
    if(!stream.fail()) {
        stream.read(_buffer, size);
        s += stream.gcount();
        var.assign(_buffer, size); // unnessasary copy we could get rid of if reqd
    }
    return s;
}

// write
template<>
unsigned write_var(std::ostream& stream, std::string const& var) {

    int32_t size=static_cast<int32_t>(var.size());
    if( size < 0 || size > 80 ) {
        ska::panda::Error e("SigProc::write: illegal size of string: ");
        e << size;
        throw e;
    }
    // write out the size of the string
    unsigned s = write_var(stream, size);
    stream.write(var.data(), size);
    return size + s;
}    

std::istream& operator>>(std::istream& stream, SigProcLabel& var)
{
    get_var(stream, static_cast<std::string&>(var));
    return stream;
}

std::ostream& operator<<(std::ostream& stream, SigProcLabel const& var)
{
    write_var(stream, static_cast<std::string const&>(var));
    return stream;
}
#define HEADER_VAR(header)  _ ## header 
#define HEADER_LINE(line) if (str == #line) { \
    _size += get_var(stream, HEADER_VAR(line));					\
    std::cout << str << ":" << HEADER_VAR(line) << std::endl; \
                          }
#define HEADER_WRITE_LINE(line) { SigProcLabel li(#line); \
                                  stream << li; \
                                  _size += write_var(stream, HEADER_VAR(line)) + li.size(); \
                                }
                        
SigProcHeader::SigProcHeader()
{
    reset();
}

SigProcHeader::~SigProcHeader()
{
}

bool SigProcHeader::operator==(SigProcHeader const& h) const
{
    return _nchans == h._nchans 
        && _nbits == h._nbits
        && _nbeams == h._nbeams
        && _ibeam == h._ibeam
        && _frequency_table[0]== h._frequency_table[0]
        && _foff == h._foff
        && _tsamp == h._tsamp
        && _tstart == h._tstart
        && _nifs == h._nifs
        && _frequency_table.size() == h._frequency_table.size()
    ;
}

bool SigProcHeader::operator!=(SigProcHeader const& h) const
{
    return !(h==*this);
}

void SigProcHeader::set_number_of_channels(int n)
{
    _nchans = n;
}

int SigProcHeader::number_of_channels() const {
    return _nchans;
}

void SigProcHeader::set_number_of_bits(int n)
{
    _nbits = n;
}

void SigProcHeader::set_number_of_intermediate_frequencies(int nifs)
{
    _nifs = nifs;
}

int SigProcHeader::number_of_intermediate_frequencies() const
{
    return _nifs;
}

int SigProcHeader::number_of_bits() const {
    return _nbits;
}

void SigProcHeader::set_start_time(typename ska::cheetah::utils::ModifiedJulianClock::time_point const& timestamp)
{
    _tstart = timestamp.time_since_epoch().count();
}

std::vector<SigProcHeader::FrequencyType> const& SigProcHeader::channel_frequencies() const
{
    return _frequency_table;
}

SigProcHeader::FrequencyType SigProcHeader::start_frequency() const
{
    return _frequency_table[0];
}

SigProcHeader::FrequencyType SigProcHeader::channel_width() const
{
    return _foff;
}

typename ska::cheetah::utils::ModifiedJulianClock::time_point SigProcHeader::start_time() const {
    return ska::cheetah::utils::ModifiedJulianClock::time_point(ska::cheetah::utils::julian_day(_tstart));
}

void SigProcHeader::set_sample_interval(typename SigProcHeader::TimeType const& duration)
{
    _tsamp = duration.value();
}

typename SigProcHeader::TimeType SigProcHeader::sample_interval() const {
    return TimeType::from_value(_tsamp);
}

void SigProcHeader::set_source_name(std::string const& s)
{
    _source_name = s;
}

std::string const& SigProcHeader::source_name() const
{
    return _source_name;
}

ska::panda::Error SigProcHeader::parse_error(std::string const& s) const
{
    ska::panda::Error e("SigProcHeader: ");
    e << s; // this operator does not throw 
    return e;
}

void SigProcHeader::reset() 
{
    _size = 0U;

    _rawdatafile.clear();
    _source_name.clear();
    _machine_id = -1;
    _telescope_id = -1;
    _data_type = -1;
    _nchans = -1;
    _nbits = 0;
    _nifs = -1;
    _scan_number = 0;

    _barycentric = 0;
    _pulsarcentric = 0;

    _tstart = 0.0f;
    _mjdobs = 0.0f;
    _tsamp = 0.0f;
    _frequency_table.clear();
    _frequency_table.emplace_back(FrequencyType());
    _foff *= 0.0f;
    _refdm = 0.0f;
    _az_start = 0.0f;
    _za_start = 0.0f;
    _src_raj = 0.0f;
    _src_dej = 0.0f;

    _gal_l = 0.0f;
    _gal_b = 0.0f;
    _header_tobs = 0.0f;
    _raw_fch1 = 0.0f;
    _raw_foff = 0.0f;

    _npuls = 0l;
    _nbeams = 0;
    _ibeam = 0;

    _period = 0.0f;
    _nbins = 0;

    _srcl = 0.0f;
    _srcb = 0.0f;
    _ast0 = 0.0f;
    _lst0 = 0.0f;
    _wapp_scan_number = 0l;
}

int SigProcHeader::telescope_id() const
{
    return _telescope_id;
}

void SigProcHeader::set_telescope_id(int id)
{
    _telescope_id = id;
}

void SigProcHeader::read(std::istream & stream)
{
    reset();

    int channel_index = 0;

    // try to read in the first line of the header
    SigProcLabel str;
    _line_count = 1;
    stream >> str;
    _size = str.size();

    if (std::strcmp(str.c_str(),"HEADER_START") !=0) {
        auto e = parse_error("expecting HEADER_START got ");
        e << str << '"';
        throw e;
    }

        
    try {
        /* loop over and read remaining header lines until HEADER_END reached */
        while (1) {
            ++_line_count;
            stream >> str;
            _size += str.size();

            if( str.find(std::string("HEADER_END") ) != std::string::npos) {
                std::cout << "End of header" << std::endl;
                break;
            }
            if( str == std::string("FREQUENCY_START") ) {
                _frequency_table.reserve(4096); // try to avoaid too many reallocs
                channel_index = 0;
            } 
            else if( str == std::string("FREQUENCY_END") ) {
                if(!_nchans) { _nchans = _frequency_table.size(); }
            }
            else if( str == "fchannel") {
                _size += get_var(stream, _frequency_table[channel_index++]);
                _foff *= 0.0f; /* set to 0 // try to avoaid too many reallocs.0 to signify that a table is in use */
            }
            else if( str == std::string("fch1") ) {
                // alternative to FREQUENCY_START
                _size += get_var(stream, _frequency_table[0]);
            }
            else HEADER_LINE(rawdatafile)
            else HEADER_LINE(source_name)
            else HEADER_LINE(az_start)
            else HEADER_LINE(za_start)
            else HEADER_LINE(src_raj)
            else HEADER_LINE(src_dej)
            else HEADER_LINE(tstart)
            else HEADER_LINE(tsamp)
            else HEADER_LINE(period)
            else HEADER_LINE(foff)
            else HEADER_LINE(nchans)
            else HEADER_LINE(telescope_id)
            else HEADER_LINE(machine_id)
            else HEADER_LINE(data_type)
            else HEADER_LINE(ibeam)
            else HEADER_LINE(nbeams)
            else HEADER_LINE(nbits)
            else HEADER_LINE(nbins)
            else HEADER_LINE(nifs)
            else HEADER_LINE(barycentric)
            else HEADER_LINE(pulsarcentric)
            else HEADER_LINE(refdm)
            else HEADER_LINE(npuls)
            else {
                throw parse_error("SigProcHeader - unknown parameter");
            }
        }
    } catch(ska::panda::Error& e) 
    {
        e << " : on line " << _line_count;
        throw e;
    }
}

unsigned SigProcHeader::size() const {
    return _size;
}

void SigProcHeader::write(std::ostream & stream) const {

    // Write header
    const SigProcLabel start("HEADER_START");
    const SigProcLabel end("HEADER_END");
    stream << start;
    _size = start.size(); 

    if(_machine_id >=0)  { HEADER_WRITE_LINE(machine_id); }
    if(_telescope_id >= 0) { HEADER_WRITE_LINE(telescope_id); }
    if(_data_type>=0) { HEADER_WRITE_LINE(data_type); }   // Channelised Data

    // Need to be parametrised ...
    if(_source_name.size()) { HEADER_WRITE_LINE(source_name); }
    if(_src_raj>0) { HEADER_WRITE_LINE(src_raj); } // Write J2000 RA
    if(_src_dej>0) { HEADER_WRITE_LINE(src_dej); } // Write J2000 Dec 
    if(_foff.value() != 0)  { 
        HEADER_WRITE_LINE(foff);
        if(_frequency_table.size() == 1 ) {
            SigProcLabel li("fch1");
            stream << li;
            _size += write_var(stream, _frequency_table[0]) + li.size();
        }
    }
    HEADER_WRITE_LINE(nchans);
    HEADER_WRITE_LINE(tsamp);
    HEADER_WRITE_LINE(nbits);
    HEADER_WRITE_LINE(tstart);
    if(_nifs>=0) { HEADER_WRITE_LINE(nifs); }         // Polarisation channels.
    stream << end;
    _size += end.size(); 
}    

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
