#include "arecibo/sigproc/WriterConfig.h"


namespace arecibo {
namespace alfaburst {
namespace sigproc {


WriterConfig::WriterConfig()
    : ska::panda::ConfigModule("sigproc writer")
{
}

WriterConfig::~WriterConfig()
{
}

void WriterConfig::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
      ("output-dir", boost::program_options::value<std::string>(&_dir), "the directory to write output files to");
}

std::string const& WriterConfig::dir() const
{
    return _dir;
}

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
