#ifndef CHEETAH_SIGPROC_SIGPROCHEADER_H
#define CHEETAH_SIGPROC_SIGPROCHEADER_H

#include "cheetah/utils/chrono.h"
#include <panda/Error.h>
#include <boost/units/systems/si/time.hpp>
#include <boost/units/systems/si/frequency.hpp>
#include <boost/units/quantity.hpp>
#include <istream>
#include <vector>

namespace arecibo {
namespace alfaburst {
namespace sigproc {

/**
 * @brief
 *    struct for the header information of a SigProc file
 */

class SigProcHeader
{
    public:
        typedef boost::units::quantity<boost::units::si::time, double> TimeType;
        typedef boost::units::quantity<boost::units::si::frequency, double> FrequencyType;

    public:
        SigProcHeader();
        ~SigProcHeader();

        /**
         * @brief read in header data from the provided stream 
         * @details will throw if the data stream is not in the expected format or eof
         */
        void read(std::istream & stream);
        /**
         * @brief write header data to the provided stream 
         * @details 
         */
        void write(std::ostream & stream) const;

       /**
        * @brief reset header variables to null/invalid values
        */
        void reset();

       /**
        * @brief set the number of channels
        */
        void set_number_of_channels(int);

       /**
        * @brief get the number of channels
        */
        int number_of_channels() const;

       /**
        * @brief set the number of bits
        */
        void set_number_of_bits(int);

       /**
        * @brief get the number of bits
        */
        int number_of_bits() const;

       /**
        * @brief get the number of intermediate frequencies
        */
        int number_of_intermediate_frequencies() const;

       /**
        * @brief set the number of intermediate frequencies
        */
        void set_number_of_intermediate_frequencies(int);

       /**
        * @brief set the start_time (corresponding to the time of the first sample
        */
        void set_start_time(typename ska::cheetah::utils::ModifiedJulianClock::time_point const&);

        typename ska::cheetah::utils::ModifiedJulianClock::time_point start_time() const;

       /**
        * @brief set the time diration of each sample
        */
        void set_sample_interval(TimeType const& duration);

       /**
        * @brief return the time diration of each sample
        */
        TimeType sample_interval() const;

       /**
        * @brief set the name of the source
        */
        void set_source_name(std::string const&);

       /**
        * @brief get the identifier for the telescope
        */
        int telescope_id() const;

       /**
        * @brief set the identifier for the telescope
        */
        void set_telescope_id(int);

       /**
        * @brief return the source name
        */
        std::string const& source_name() const;

       /**
        * @brief return the size in bytes of the header
        *         n.b. only valid after a reading a header file
        */
        unsigned size() const;

       /**
        * @brief returns true if the data formats match
        * @details source names/machine names are ignored
        *          this only matches data formatting types and offsets
        */
        bool operator==(SigProcHeader const&) const;

       /**
        * @brief returns true if the data format meta data does not match
        * @details see caveats in operator==
        */
        bool operator!=(SigProcHeader const&) const;

       /**
        * @brief returns the frequencies each channel represents
        */
        std::vector<FrequencyType> const& channel_frequencies() const;

       /**
        * @brief returns the first channel frequency (if set as a seperate parameter and not in channel_freq table
        */
        FrequencyType start_frequency() const;

       /**
        * @brief returns the channel width where it is constant. If not constsnt will return 0
        */
        FrequencyType channel_width() const;

    private:
        ska::panda::Error parse_error(std::string const&) const;

    private:
        // no public intrefcae at present as these are ill defined and it is unknown if needed
        // should be provided with appropriate getters/setters

        std::string _rawdatafile; 
        int _machine_id, _telescope_id, _data_type, _scan_number;
        int _barycentric, _pulsarcentric;
        double _mjdobs,_refdm,_az_start,_za_start,_src_raj,_src_dej;
        double _gal_l,_gal_b,_header_tobs,_raw_fch1,_raw_foff;
        int _nbeams, _ibeam;

        double _srcl,_srcb;
        double _ast0, _lst0;
        long _wapp_scan_number;

        long int _npuls;

        double _period;
        int _nbins;

    private:
        int _nifs; // Number of Intermediate Frequency signals
        FrequencyType _foff;
        std::vector<FrequencyType> _frequency_table;
        int _nchans, _nbits;
        double _tstart; // Modified Julian Date format
        double _tsamp;  // sample time (in seconds
        std::string _source_name;

        unsigned _line_count;
        mutable unsigned _size; // byte size of the header
};

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo

#endif // CHEETAH_SIGPROC_SIGPROCHEADER_H 
