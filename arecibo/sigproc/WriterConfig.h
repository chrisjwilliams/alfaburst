#ifndef SKA_CHEETAH_SIGPROC_WRITERCONFIG_H
#define SKA_CHEETAH_SIGPROC_WRITERCONFIG_H


#include "panda/ConfigModule.h"

namespace arecibo {
namespace alfaburst {
namespace sigproc {

/**
 * @brief
 *    Configuration options for a SigProcWriter
 * @details
 * 
 */

class WriterConfig : public ska::panda::ConfigModule
{
    public:
        WriterConfig();
        ~WriterConfig();

        std::string const& dir() const;

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        std::string _dir;
};


} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo

#endif // SKA_CHEETAH_SIGPROC_WRITERCONFIG_H 
