#ifndef SKA_CHEETAH_SIGPROC_SIGPROCFILESTREAM_H
#define SKA_CHEETAH_SIGPROC_SIGPROCFILESTREAM_H

#include "arecibo/data/TimeFrequency.h"
#include "arecibo/sigproc/SigProcHeader.h"
#include "arecibo/sigproc/Config.h"
#include "panda/Producer.h"
#include <fstream>
#include <string>

namespace arecibo {
namespace alfaburst {
namespace sigproc {
/**
 * @class SigProcFileStream
 * 
 * @brief
 *    Read in a SigProc format input file and generate a series of data chunks
 */

template<typename Producer>
class SigProcFileStream : public ska::panda::Producer<Producer, data::TimeFrequency<Cpu>>
{
    public:
        SigProcFileStream(Config const& config);
        ~SigProcFileStream();

        /**
         * @returns true if all data has been consumed
         */
        bool process();

    protected:
        /**
         * @brief return the number of samples (time * channels) in the currently opened file
         * @details actual data samples = this * number of channels.
         */
        std::size_t number_of_data_samples() const;

    private:
        void transfer_header_info(data::TimeFrequency<Cpu>& chunk);
        bool fill_chunk(data::TimeFrequency<Cpu>& chunk, std::size_t offset, data::TimeFrequency<Cpu>::Iterator& it );

    private:
        bool next_file();
        std::ifstream _file_stream;

    private:
        Config const& _config;
        SigProcHeader _header;
        ska::cheetah::utils::ModifiedJulianClock::time_point _start_time;

        mutable std::size_t _nsamples; // calculated number of samples from header info and file size
        std::vector<std::string> _filenames;
        std::vector<std::string>::const_iterator _file_it;
};

class SigProcReader : public SigProcFileStream<SigProcReader>
{
    public:
        SigProcReader(Config const& config);
};

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
#include "arecibo/sigproc/detail/SigProcFileStream.cpp"

#endif // SKA_CHEETAH_SIGPROC_SIGPROCFILESTREAM_H 
