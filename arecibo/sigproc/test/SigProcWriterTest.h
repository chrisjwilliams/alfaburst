#ifndef SKA_CHEETAH_SIGPROC_TEST_SIGPROCWRITERTEST_H
#define SKA_CHEETAH_SIGPROC_TEST_SIGPROCWRITERTEST_H

#include <gtest/gtest.h>

namespace arecibo {
namespace alfaburst {
namespace sigproc {
namespace test {

/**
 * @brief
 * 
 * @details
 * 
 */
class SigProcWriterTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        SigProcWriterTest();

        ~SigProcWriterTest();

    private:
};

} // namespace test
} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo

#endif // SKA_CHEETAH_SIGPROC_TEST_SIGPROCWRITERTEST_H 
