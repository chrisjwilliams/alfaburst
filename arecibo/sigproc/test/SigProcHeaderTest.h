#ifndef SKA_CHEETAH_SIGPROC_TEST_SIGPROCHEADERTEST_H
#define SKA_CHEETAH_SIGPROC_TEST_SIGPROCHEADERTEST_H

#include <gtest/gtest.h>

namespace arecibo {
namespace alfaburst {
namespace sigproc {
namespace test {

/**
 * @brief
 * 
 * @details
 * 
 */
class SigProcHeaderTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        SigProcHeaderTest();

        ~SigProcHeaderTest();

    private:
};

} // namespace test
} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo

#endif // SKA_CHEETAH_SIGPROC_TEST_SIGPROCHEADERTEST_H 
