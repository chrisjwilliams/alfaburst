#ifndef SKA_CHEETAH_SIGPROC_TEST_SIGPROCFILESTREAMTEST_H
#define SKA_CHEETAH_SIGPROC_TEST_SIGPROCFILESTREAMTEST_H

#include <gtest/gtest.h>

namespace arecibo {
namespace alfaburst {
namespace sigproc {
namespace test {

/**
 * @brief
 * 
 * @details
 * 
 */
class SigProcFileStreamTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        SigProcFileStreamTest();

        ~SigProcFileStreamTest();

    private:
};

} // namespace test
} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo

#endif // SKA_CHEETAH_SIGPROC_TEST_SIGPROCFILESTREAMTEST_H 
