#include "arecibo/sigproc/test/SigProcFileStreamTest.h"
#include "arecibo/sigproc/SigProcFileStream.h"

#include "panda/DataManager.h"
#include "panda/test/gtest.h"

namespace arecibo {
namespace alfaburst {
namespace sigproc {
namespace test {


SigProcFileStreamTest::SigProcFileStreamTest()
    : ::testing::Test()
{
}

SigProcFileStreamTest::~SigProcFileStreamTest()
{
}

void SigProcFileStreamTest::SetUp()
{
}

void SigProcFileStreamTest::TearDown()
{
}

TEST_F(SigProcFileStreamTest, test_empty_file_name)
{
    Config config;
    SigProcReader stream(config);
    ska::panda::DataManager<SigProcReader> chunk_manager(stream);
    ASSERT_TRUE(stream.process()); // end of stream
}

TEST_F(SigProcFileStreamTest, test_single_valid_file_defined)
{
    Config config;
    config.set_sigproc_files(ska::panda::test::test_file("valid_sigproc.sigproc"));
    SigProcReader stream(config);
    ska::panda::DataManager<SigProcReader> chunk_manager(stream);
    ASSERT_FALSE(stream.process());

    std::tuple<std::shared_ptr<data::TimeFrequency<Cpu>>> data_tuple=chunk_manager.next();
    data::TimeFrequency<Cpu>& data = *(std::get<0>(data_tuple));
    ASSERT_EQ(250368U, data.number_of_samples());
    ASSERT_EQ(std::size_t(2), data.number_of_channels());
}

TEST_F(SigProcFileStreamTest, test_single_invalid_file_defined)
{
    Config config;
    config.set_sigproc_files(ska::panda::test::test_file("invalid_sigproc.sigproc"));
    SigProcReader stream(config);
    ska::panda::DataManager<SigProcReader> chunk_manager(stream);
    ASSERT_THROW(stream.process(), ska::panda::Error);
    ASSERT_TRUE(stream.process()); // end of stream
}

TEST_F(SigProcFileStreamTest, test_invalid_file_followed_by_valid_file)
{
    Config config;
    config.set_sigproc_files(std::vector<std::string>({ska::panda::test::test_file("invalid_sigproc.sigproc"), ska::panda::test::test_file("valid_sigproc.sigproc") }));
    SigProcReader stream(config);
    ska::panda::DataManager<SigProcReader> chunk_manager(stream);
    ASSERT_THROW(stream.process(), ska::panda::Error);
    ASSERT_FALSE(stream.process()); // valid data
}

TEST_F(SigProcFileStreamTest, test_chunk_samples_from_config_preset)
{
    for(unsigned i=1; i<5; ++i) {
        Config config;
        config.set_sigproc_files(ska::panda::test::test_file("valid_sigproc.sigproc"));
        config.set_chunk_samples(i);

        SigProcReader stream(config);
        ska::panda::DataManager<SigProcReader> chunk_manager(stream);
        ASSERT_FALSE(stream.process()) << i;
        std::tuple<std::shared_ptr<data::TimeFrequency<Cpu>>> data = chunk_manager.next();
        ASSERT_EQ(i, std::get<0>(data)->number_of_samples());
        ASSERT_EQ(std::size_t(2), std::get<0>(data)->number_of_channels());
        ASSERT_EQ( std::get<0>(data)->end() - std::get<0>(data)->begin(), i * std::get<0>(data)->number_of_channels());
    }
}

} // namespace test
} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
