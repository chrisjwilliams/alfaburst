#include "arecibo/sigproc/test/SigProcWriterTest.h"
#include "arecibo/sigproc/SigProcWriter.h"
#include "arecibo/sigproc/SigProcFileStream.h"
#include "panda/test/TestDir.h"
#include <climits>
#include "unistd.h"


namespace arecibo {
namespace alfaburst {
namespace sigproc {
namespace test {


SigProcWriterTest::SigProcWriterTest()
    : ::testing::Test()
{
}

SigProcWriterTest::~SigProcWriterTest()
{
}

void SigProcWriterTest::SetUp()
{
}

void SigProcWriterTest::TearDown()
{
}

TEST_F(SigProcWriterTest, test_write_read)
{
    unsigned samples = 20;
    // create a tmp directoy name
    for(unsigned i=1; i<9046; i+=1024) { // try different channel numbers
        ska::panda::test::TestDir tmp_dir;
        ASSERT_NO_THROW(tmp_dir.create());
        {
            data::TimeFrequency<Cpu> tf1(samples, i);
            data::TimeFrequency<Cpu> tf2(samples, i);
            SigProcWriter writer(tmp_dir.dir_name());
            writer << tf1;
            writer << tf2;
        } // should flush buffers to OS on leaving scope
        sync(); // ensure the OS actually writes to disc
        auto it = boost::filesystem::directory_iterator(tmp_dir.path());
        ASSERT_FALSE(it == boost::filesystem::directory_iterator());
        
        Config config;
        ASSERT_TRUE(boost::filesystem::exists(it->path()));
        config.set_sigproc_files(it->path().native());
        config.set_chunk_samples(3*samples); // should be reduced
        SigProcReader stream(config);
        ska::panda::DataManager<SigProcReader> chunk_manager(stream);
        ASSERT_TRUE(stream.process()) << "channels=" << i; // expect to of tried opening a new file (and fail) to find the missing data
        std::tuple<std::shared_ptr<data::TimeFrequency<Cpu>>> data = chunk_manager.next();
        ASSERT_EQ(i, std::get<0>(data)->number_of_channels());
        ASSERT_EQ(2*samples, std::get<0>(data)->number_of_samples()) << "channel=" << i;
    }
}

} // namespace test
} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
