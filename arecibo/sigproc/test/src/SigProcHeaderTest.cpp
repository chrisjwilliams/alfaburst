#include "arecibo/sigproc/test/SigProcHeaderTest.h"
#include "arecibo/sigproc/SigProcHeader.h"
#include "cheetah/utils/chrono.h"

#include <sstream>

namespace arecibo {
namespace alfaburst {
namespace sigproc {
namespace test {


SigProcHeaderTest::SigProcHeaderTest()
    : ::testing::Test()
{
}

SigProcHeaderTest::~SigProcHeaderTest()
{
}

void SigProcHeaderTest::SetUp()
{
}

void SigProcHeaderTest::TearDown()
{
}

// save a header to a stream and then restore it into a fresh SigProcHeader object which is returned
SigProcHeader save_restore(SigProcHeader const& h)
{
    std::stringstream ss;
    h.write(ss); 
    SigProcHeader r;
    r.read(ss);
    return r;
}

TEST_F(SigProcHeaderTest, number_of_channels)
{
    SigProcHeader h;
    ASSERT_EQ(-1, h.number_of_channels());
    h.set_number_of_channels(332);
    ASSERT_EQ(332, h.number_of_channels());
    SigProcHeader h2 = save_restore(h);
    ASSERT_EQ(h2.number_of_channels(), h.number_of_channels());

    h.reset();
    ASSERT_EQ(-1, h.number_of_channels());
}

TEST_F(SigProcHeaderTest, number_of_bits)
{
    SigProcHeader h;
    ASSERT_EQ(0, h.number_of_bits());
    h.set_number_of_bits(32);
    ASSERT_EQ(32, h.number_of_bits());
    SigProcHeader h2 = save_restore(h);
    ASSERT_EQ(h2.number_of_bits(), h.number_of_bits());
    h.reset();
    ASSERT_EQ(0, h.number_of_bits());
}

TEST_F(SigProcHeaderTest, test_start_time)
{
    ska::cheetah::utils::TimePoint<ska::cheetah::utils::ModifiedJulianClock> epoch(ska::cheetah::utils::julian_day(0));
    ska::cheetah::utils::TimePoint<ska::cheetah::utils::ModifiedJulianClock> t(ska::cheetah::utils::julian_day(12));
    SigProcHeader h;
    ASSERT_EQ(epoch, h.start_time());
    h.set_start_time(t);
    SigProcHeader h2 = save_restore(h);
    ASSERT_EQ(h2.start_time(), h.start_time());
    ASSERT_EQ(t, h.start_time());
    h.reset();
    ASSERT_EQ(epoch, h.start_time());
}

TEST_F(SigProcHeaderTest, test_sample_interval)
{
    SigProcHeader::TimeType zero = SigProcHeader::TimeType::from_value(0.0);
    SigProcHeader::TimeType t = SigProcHeader::TimeType::from_value(20.0);
    SigProcHeader h;
    ASSERT_EQ(zero, h.sample_interval());
    h.set_sample_interval(t);
    SigProcHeader h2 = save_restore(h);
    ASSERT_EQ(h2.sample_interval(), h.sample_interval());
    ASSERT_EQ(t, h.sample_interval());
    h.reset();
    ASSERT_EQ(zero, h.sample_interval());
}

TEST_F(SigProcHeaderTest, test_source_name)
{
    std::string s("abc");
    std::string empty;

    SigProcHeader h;
    ASSERT_EQ(empty, h.source_name());
    h.set_source_name(s);
    ASSERT_EQ(s, h.source_name());
    SigProcHeader h2 = save_restore(h);
    ASSERT_EQ(h2.source_name(), h.source_name());
    h.reset();
    ASSERT_EQ(empty, h.source_name());
}

TEST_F(SigProcHeaderTest, test_size)
{
    SigProcHeader h;
    std::stringstream ss;
    h.write(ss); 

    SigProcHeader r;
    r.read(ss);
    
    ASSERT_EQ(r.size(), ss.str().size());
    ASSERT_EQ(r.size(), h.size());
}

} // namespace test
} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
