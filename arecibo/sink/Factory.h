/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2018 Oxford Astrophysics
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef ARECIBO_ALFABURST_SINK_FACTORY_H
#define ARECIBO_ALFABURST_SINK_FACTORY_H

#include "sigproc/Header.h"
#include "arecibo/data/TimeFrequency.h"
#include "cheetah/exporters/DataExport.h"
#include "cheetah/pipeline/DataExport.h"
#include "cheetah/data/SpCcl.h"

namespace arecibo {
namespace alfaburst {
namespace sink {
class Config;

/**
 * @brief Factory for creating data sinks
 */

struct DataExportTraits : public ska::cheetah::pipeline::DefaultExportTraits
{
    typedef sigproc::Header SigProcHeader;
};

template<typename NumericalT>
//class Factory : public ska::cheetah::exporters::DataExport<data::TimeFrequency<Cpu, NumericalT>, ska::cheetah::data::SpCcl>
class Factory : public ska::cheetah::pipeline::DataExport<NumericalT, DataExportTraits>
{
        //typedef ska::cheetah::exporters::DataExport<data::TimeFrequency<Cpu, NumericalT>, ska::cheetah::data::SpCcl>  BaseT;
        typedef ska::cheetah::pipeline::DataExport<NumericalT, DataExportTraits> BaseT;

    public:
        Factory(Config const& config);
        ~Factory();
};


template<>
class Factory<uint8_t> : public ska::cheetah::pipeline::DataExport<uint8_t, DataExportTraits>
{
        typedef ska::cheetah::pipeline::DataExport<uint8_t, DataExportTraits> BaseT;

    public:
        Factory(Config const& config);
        ~Factory();
};


} // namespace sink
} // namespace alfaburst
} // namespace arecibo
#include "detail/Factory.cpp"

#endif // ARECIBO_ALFABURST_SINK_FACTORY_H
