#ifndef ARECIBO_ALFABURST_HEADER_H
#define ARECIBO_ALFABURST_HEADER_H

#include "pss/astrotypes/sigproc/Header.h"

namespace arecibo {
namespace alfaburst {
namespace sigproc {

/**
 * @brief
 *    Custom Header for sigproc output files
 * @details
 * 
 */
class Header : public pss::astrotypes::sigproc::Header
{
        typedef pss::astrotypes::sigproc::Header BaseT;

    public:
        Header();
        Header(Header const&);
        ~Header();
};

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_HEADER_H 
