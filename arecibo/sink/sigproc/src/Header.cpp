#include "arecibo/sink/sigproc/Header.h"


namespace arecibo {
namespace alfaburst {
namespace sigproc {


Header::Header()
{
    telescope_id(1); // arecibo
    machine_id(22);  // alfaburst
}

Header::~Header()
{
}

Header::Header(Header const& copy)
    : BaseT(copy)
{
}

} // namespace sigproc
} // namespace alfaburst
} // namespace arecibo
