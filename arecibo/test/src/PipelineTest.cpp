#include "PipelineTest.h"
#include "arecibo/AlfaBurstConfiguration.h"
#include "arecibo/receptor/AlfaBurstStream.h"
#include "arecibo/AlfaBurstPipeline.h"
#include "arecibo/emulator/Emulator.h"
#include "arecibo/emulator/SinDataModel.h"
#include "arecibo/emulator/EmulatorSettings.h"
#include "arecibo/emulator/EmulatorRunner.h"

#include "panda/test/PipelineWrapper.h"

#include <thread>
#include <chrono>

namespace arecibo {
namespace alfaburst {


PipelineTest::PipelineTest()
{
}

PipelineTest::~PipelineTest()
{
}

TEST_F(PipelineTest, dataflow_with_emulator) {

    // Setup an Emulated Data Source
    ska::panda::IpAddress send_address("127.0.0.1");
    send_address.port(0);
    EmulatorSettings settings(send_address);
    SinDataModel model;
    EmulatorRunner emulator(settings, model);
    
    // Set up a pipeline
    ska::panda::IpAddress listen_address("127.0.0.1");;
    listen_address.port(emulator.emulator().subscriber_end_point().port());
    std::cout << "connecting to port " << emulator.emulator().subscriber_end_point().port() << std::endl;
    receptor::Config config;
    config.datastream_end_point(listen_address.end_point<boost::asio::ip::udp::endpoint>());

    unsigned call_count=0;
    emulator.wait_run();
return;
    try {
        receptor::AlfaBurstStream<uint16_t> chunked_data(config);
        //ska::panda::test::PipelineWrapper<AlfaBurstPipeline> pipeline(chunked_data, [&call_count](AlfaBurstChunk&, AreciboConfigurationData&)
        AlfaBurstPipeline pipeline(chunked_data, [&call_count](data::TimeFrequency<Cpu>&, data::AreciboConfigurationData&)
                                                                                    { 
                                                                                        ++call_count;
                                                                                        std::cout << "pipeline call : " << call_count << std::endl;
                                                                                    });

        pipeline.start();
    } catch(std::exception const& e)
    {
        FAIL() << "unexpected exception" << e.what();
    } catch(...)
    {
        FAIL() << "unexpected exception";
    }
    ASSERT_EQ(3U, call_count);
}

} // namespace alfaburst
} // namespace arecibo
