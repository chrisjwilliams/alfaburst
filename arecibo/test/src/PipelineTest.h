#ifndef ARECIBO_ALFABURST_PIPELINETEST_H
#define ARECIBO_ALFABURST_PIPELINETEST_H

#include <gtest/gtest.h>

namespace arecibo {
namespace alfaburst {

/**
 * @class PipelineTest
 * 
 * @brief
 *     Test the data propogation through the Alfaburst Pipeline
 * @details
 * 
 */
class PipelineTest : public ::testing::Test
{
    public:
        PipelineTest();

        ~PipelineTest();

    private:
};

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_PIPELINETEST_H 
