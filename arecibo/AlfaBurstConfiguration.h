#ifndef ARECIBO_ALFABURST_ALFABURSTCONFIGURATION_H
#define ARECIBO_ALFABURST_ALFABURSTCONFIGURATION_H

#include "arecibo/ConfigModule.h"
#include "arecibo/config/ModulesConfig.h"
#include <cheetah/emulator/Config.h>
#include <panda/Engine.h>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>

namespace arecibo {
namespace alfaburst {

/**
 * @class AlfaBurstConfiguration
 *
 * @brief
 *    Parses the configuration information for the alfaburst pipeline
 *
 */

//class AlfaBurstConfiguration : public ska::panda::Config<ska::panda::nvidia::Cuda, ska::panda::Cpu>
class AlfaBurstConfiguration : public ConfigModule::ConfigType
{
        typedef ska::panda::Config<ska::panda::nvidia::Cuda, ska::panda::Cpu> BaseT;

    public:
        typedef ska::panda::Engine Engine;
        typedef BaseT::PoolType PoolType;
        typedef BaseT::PoolManagerType PoolManagerType;

    public:
        AlfaBurstConfiguration();
        ~AlfaBurstConfiguration();


        Engine& engine() const;

        /**
         * @brief set the computational unit (processing pipeline) names that are available
         */
        void set_pipeline_handlers(std::vector<std::string> const& handler_names);

        /**
         * @brief return the configuration for the T type module
         */
        template<typename T>
        ska::panda::PoolSelector<BaseT::PoolManagerType, T> const& module_config() const { return _modules.template config<T>(); }

        /**
         * @brief parse the command line options/config file
         * @returns 0 to indicate no parse halting options specified
         * @returns >0 to indicate parse failed or parse indicates program halt
         */

        /**
         * @brief emulated stream configuration
         */
        ska::cheetah::emulator::Config const& emulator_config() const;

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        mutable Engine _engine;
        config::ModulesConfig<BaseT::PoolManagerType>  _modules;

        // module configuration
        ska::cheetah::emulator::Config _emulator_config;
};

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_ALFABURSTCONFIGURATION_H
