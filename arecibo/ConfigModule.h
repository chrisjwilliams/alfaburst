#ifndef ARECIBO_ALFABURST_CONFIGMODULE_H
#define ARECIBO_ALFABURST_CONFIGMODULE_H

#include <panda/arch/nvidia/Nvidia.h>
#include <panda/Cpu.h>
#include <panda/ConfigModule.h>
#include <panda/Config.h>
#include <tuple>

namespace arecibo {
namespace alfaburst {

/**
 * @brief
 * 
 * @details
 * 
 */
class ConfigModule : public ska::panda::ConfigModule
{
    public:
        typedef ska::panda::Config<ska::panda::nvidia::Cuda, ska::panda::Cpu> ConfigType;
        typedef typename ConfigType::PoolManagerType PoolManagerType;

    public:
        ConfigModule();
        ~ConfigModule();

    private:
};

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_CONFIGMODULE_H 
