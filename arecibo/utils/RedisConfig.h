#ifndef ARECIBO_ALFABURST_REDISCONFIG_H
#define ARECIBO_ALFABURST_REDISCONFIG_H


#include "panda/EndpointConfig.h"

namespace arecibo {
namespace alfaburst {

/**
 * @brief
 * 
 * @details
 * 
 */
class RedisConfig : public ska::panda::EndpointConfig
{
    public:
        RedisConfig();

        ~RedisConfig();

};

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_REDISCONFIG_H 
