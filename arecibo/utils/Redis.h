#ifndef ARECIBO_ALFABURST_REDIS_H
#define ARECIBO_ALFABURST_REDIS_H

#include "RedisConfig.h"
#include <string>

struct redisContext;

namespace arecibo {
namespace alfaburst {
class RedisReply;

/**
 * @brief
 *   C++ wrapper around the arecibo Redis database
 * @details
 * 
 */
class Redis
{
    public:
        Redis(RedisConfig const&);
        ~Redis();
        
	/**
	 * @brief send a query to thr redis database
	 * @details note this method can throw if there is a connection error
	 */
        RedisReply query(std::string const& cmd);

        RedisConfig redis_config();

    private:
        void connect();

    private:
        redisContext* _context;
        RedisConfig const& _redis_config;
};

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_REDIS_H 
