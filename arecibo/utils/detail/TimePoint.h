#ifndef ARECIBO_ALFABURST_UTILS_TIMEPOINT_H
#define ARECIBO_ALFABURST_UTILS_TIMEPOINT_H
#include <chrono>
#include <ostream>


namespace arecibo {
namespace alfaburst {
namespace utils {

/**
 * @brief
 *     extension of std::chrono::time_point for chhetah clocks
 * @tparam ClockType must support the std::Clock conecpt as as weel as defining a duration type @member diff_from_system_epoch
 */
template<typename ClockType, typename Duration = typename ClockType::duration>
struct TimePoint : public std::chrono::time_point<ClockType, Duration>
{
    explicit TimePoint( const Duration& d = typename ClockType::duration() );
    explicit TimePoint( const std::chrono::system_clock::time_point& tp );
    TimePoint( const std::chrono::time_point<ClockType, Duration>& tp );
    TimePoint( std::chrono::time_point<ClockType, Duration>&& );

    operator typename std::chrono::time_point<ClockType, Duration>() const;
    operator typename std::chrono::system_clock::time_point() const;
    TimePoint<ClockType, Duration>& operator+=(Duration const&);
    TimePoint<ClockType, Duration>& operator-=(Duration const&);

    /**
     * @brief convert to a C style time struct.
     * @detials very useful if you want to output the time as a string with e.g. std::put_time
     */
    std::time_t to_time_t() const;
};

template<typename ClockType, typename Duration, typename Duration2>
TimePoint<ClockType, Duration> operator+(TimePoint<ClockType, Duration> const& lhs, Duration2 const& rhs);

template<typename ClockType, typename Duration, typename Duration2>
TimePoint<ClockType, Duration> operator-(TimePoint<ClockType, Duration> const& lhs, Duration2 const& rhs);

/**
 * @brief output a TimePoint as a string to the ostream
 * @details in ISO8601 format UTC. For other formats you must
 *          use the to_time_t() method and the std::put_time function
 */
template<typename ClockType, typename Duration>
std::ostream& operator<<(std::ostream&, TimePoint<ClockType> const& tp);

} // namespace utils
} // namespace alfaburst
} // namespace arecibo
#include "arecibo/utils/detail/TimePoint.cpp"

#endif // ARECIBO_ALFABURST_UTILS_TIMEPOINT_H 
