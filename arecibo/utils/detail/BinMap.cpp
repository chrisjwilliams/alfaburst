#include "arecibo/utils/BinMap.h"
#include <boost/units/quantity.hpp>
#include <functional>

namespace arecibo {
namespace alfaburst {
namespace utils {

namespace detail {
template<typename DataType>
struct DataTypeOperationsHelper {
    template<typename T>
    inline
    DataType operator()(DataType const& data, T const& t) const {
        return data * t;
    }
    template<typename T>
    inline
    DataType divide(DataType const& data, T const& t) const {
        return data/DataType(t);
    }
};

template<typename Unit, typename DataType>
struct DataTypeOperationsHelper<boost::units::quantity<Unit, DataType>> {
    template<typename T>
    inline
    boost::units::quantity<Unit,DataType> operator()(boost::units::quantity<Unit,DataType> const& data, T const& t) const {
        return data * static_cast<DataType>(t);
    }
    template<typename T>
    inline
    boost::units::quantity<Unit,DataType> divide(boost::units::quantity<Unit,DataType> const& data, T const& t) const {
        return data/static_cast<DataType>(t);
    }
};

} // namespace detail

template<typename DataType>
BinMap<DataType>::BinMap()
    : _n_bins(0)
    , _lower(0)
    , _width(0)
{
}

template<typename DataType>
BinMap<DataType>::~BinMap()
{
}

template<typename DataType>
BinMap<DataType>::BinMap(unsigned int number_of_bins)
    : _n_bins(number_of_bins)
    , _lower(0)
    , _width(0)
{
}

template<typename DataType>
BinMap<DataType>::BinMap(unsigned int number_of_bins, DataType start, DataType const& end)
    : _n_bins(number_of_bins)
    , _lower(std::move(start))
{
    set_upper_bound(end);
}

template<typename DataType>
void BinMap<DataType>::reset(unsigned int number_of_bins)
{
     _n_bins = number_of_bins;
}

template<typename DataType>
void BinMap<DataType>::set_bin_width(DataType width)
{
    _width = width;
    _halfwidth = detail::DataTypeOperationsHelper<DataType>().divide(width, 2);
}

template<typename DataType>
void BinMap<DataType>::set_bounds(DataType lower, DataType upper)
{
    _lower = lower;
    set_upper_bound(upper);
}

template<typename DataType>
void BinMap<DataType>::set_lower_bound(DataType const start)
{
    _lower = start;
}

template<typename DataType>
void BinMap<DataType>::set_upper_bound(DataType const& end)
{
    set_bin_width( detail::DataTypeOperationsHelper<DataType>().divide((end - _lower),_n_bins));
}

template<typename DataType>
inline DataType BinMap<DataType>::lower_bound() const
{
    return _lower;
}

template<typename DataType>
inline DataType BinMap<DataType>::upper_bound() const
{
    return last_bin_assignment_value() + _halfwidth;
}

template<typename DataType>
inline DataType BinMap<DataType>::bin_assignment_number(int index) const
{
    return _lower + _halfwidth + detail::DataTypeOperationsHelper<DataType>()(_width, index);
}

template<typename DataType>
DataType BinMap<DataType>::bin_start(unsigned int index) const
{
   return bin_assignment_number(index) - _halfwidth;
}

template<typename DataType>
DataType BinMap<DataType>::bin_end(unsigned int index) const
{
   return bin_assignment_number(index) + _halfwidth;
}

template<typename DataType>
unsigned int BinMap<DataType>::bin_index(DataType const& value) const
{
    return (int)(0.5 + ((value - _lower)/ _width ));
}

template<typename DataType>
inline unsigned BinMap<DataType>::last_bin_index() const
{
    return _n_bins-1;
}

template<typename DataType>
DataType BinMap<DataType>::last_bin_assignment_value() const 
{ 
    return _lower + detail::DataTypeOperationsHelper<DataType>()(_width, last_bin_index()) + _halfwidth;
}

template<typename DataType>
DataType BinMap<DataType>::first_bin_assignment_value() const 
{ 
    return _lower + _halfwidth;
}

template<typename DataType>
DataType BinMap<DataType>::bin_width() const 
{
    return _width;
}

template<typename DataType>
unsigned int BinMap<DataType>::number_of_bins() const
{
    return _n_bins;
}

template<typename DataType>
bool BinMap<DataType>::equal(const BinMap<DataType>& map) const
{
     return map._lower == _lower && map._n_bins == _n_bins && map._width == _width;
}

template<typename DataType>
bool operator==(const BinMap<DataType>& map_1, const BinMap<DataType>& map_2)
{
     return map_1.equal(map_2);
}

} // namespace utils
} // namespace alfaburst
} // namespace arecibo

// hashing function to allow storage of type in STL containers
namespace std {
    template<typename DataType>
    struct hash<arecibo::alfaburst::utils::BinMap<DataType>> {
        std::size_t operator()(const arecibo::alfaburst::utils::BinMap<DataType>& map) const {
            return std::hash<unsigned>()(map.number_of_bins()) ^ std::hash<DataType>()(map.lower_bound()) ^ (std::hash<DataType>()(map.bin_width()) << 4);
        }
    };
} // namespace std
