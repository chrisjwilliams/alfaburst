#include "arecibo/utils/detail/TimePoint.h"
#include <iomanip>
#include <ctime>

namespace arecibo {
namespace alfaburst {
namespace utils {

template<typename ClockType, typename Duration>
TimePoint<ClockType, Duration>::TimePoint( const Duration& d )
    : std::chrono::time_point<ClockType, Duration>(d)
{
}

template<typename ClockType, typename Duration>
TimePoint<ClockType, Duration>::TimePoint( const std::chrono::system_clock::time_point& tp ) 
    : std::chrono::time_point<ClockType, Duration>(std::chrono::duration_cast<typename ClockType::duration>(tp.time_since_epoch()) + ClockType::diff_from_system_epoch)
{
}

template<typename ClockType, typename Duration>
TimePoint<ClockType, Duration>::TimePoint( const std::chrono::time_point<ClockType, Duration>& tp )
    : std::chrono::time_point<ClockType, Duration>(tp)
{
}

template<typename ClockType, typename Duration>
TimePoint<ClockType, Duration>::TimePoint( std::chrono::time_point<ClockType, Duration>&& tp )
    : std::chrono::time_point<ClockType, Duration>(std::move(tp))
{
}

template<typename ClockType, typename Duration>
TimePoint<ClockType, Duration>::operator typename std::chrono::time_point<ClockType, Duration>() const
{
    return *this;
}

template<typename ClockType, typename Duration>
TimePoint<ClockType, Duration>::operator typename std::chrono::system_clock::time_point() const
{
    auto dur = this->time_since_epoch() - ClockType::diff_from_system_epoch;
    return std::chrono::system_clock::time_point(std::chrono::duration_cast<std::chrono::system_clock::duration>(dur));
}

template<typename ClockType, typename Duration>
std::time_t TimePoint<ClockType, Duration>::to_time_t() const
{
    return std::chrono::system_clock::to_time_t(static_cast<std::chrono::system_clock::time_point>(*this));
}

template<typename ClockType, typename Duration>
std::ostream& operator<<(std::ostream& os, TimePoint<ClockType, Duration> const& tp)
{
    std::time_t tt = tp.to_time_t();
    //os << std::put_time(std::gmtime(&tt), "%F %T"); // not implemented till gcc 5.0
    os << asctime(std::gmtime(&tt));
    return os;
}

template<typename ClockType, typename Duration>
TimePoint<ClockType, Duration>& TimePoint<ClockType, Duration>::operator+=(Duration const& d)
{
    std::chrono::time_point<ClockType, Duration>::operator+=(d);
    return *this;
}

template<typename ClockType, typename Duration>
TimePoint<ClockType, Duration>& TimePoint<ClockType, Duration>::operator-=(Duration const& d)
{
    std::chrono::time_point<ClockType, Duration>::operator-=(d);
    return *this;
}

template<typename ClockType, typename Duration, typename Duration2>
TimePoint<ClockType, Duration> operator+(TimePoint<ClockType, Duration> const& lhs, Duration2 const& rhs) {
    return TimePoint<ClockType, Duration>(static_cast<std::chrono::time_point<ClockType, Duration>>(lhs) + rhs);
}

template<typename ClockType, typename Duration, typename Duration2>
TimePoint<ClockType, Duration> operator-(TimePoint<ClockType, Duration> const& lhs, Duration2 const& rhs) {
    return TimePoint<ClockType, Duration>(static_cast<std::chrono::time_point<ClockType, Duration>>(lhs) - rhs);
}

} // namespace utils
} // namespace alfaburst
} // namespace arecibo
