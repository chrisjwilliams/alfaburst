#include "arecibo/utils/RedisReply.h"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wc99-extensions"
#include <hiredis/hiredis.h>
#pragma GCC diagnostic pop
#include <cassert>

namespace arecibo {
namespace alfaburst {


RedisReply::RedisReply(redisReply* reply)
    : _reply(reply)
{
    assert(_reply);
}

RedisReply::~RedisReply()
{
    freeReplyObject(_reply);
}

bool RedisReply::is_error() const
{
    return (REDIS_REPLY_ERROR == _reply->type);
}

std::string RedisReply::str() const
{
    if(_reply->type == REDIS_REPLY_STRING || _reply->type == REDIS_REPLY_STATUS) {
        return std::string(_reply->str, _reply->len);
    }
    return std::string();
}

} // namespace alfaburst
} // namespace arecibo
