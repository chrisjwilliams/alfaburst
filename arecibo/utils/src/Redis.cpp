#include "arecibo/utils/Redis.h"
#include "arecibo/utils/RedisReply.h"
#include "panda/Error.h"
#include "panda/Log.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wc99-extensions"
#include <hiredis/hiredis.h>
#pragma GCC diagnostic pop

namespace arecibo {
namespace alfaburst {


Redis::Redis(RedisConfig const& redis_config)
    : _context(nullptr)
    , _redis_config(redis_config)
{
}

Redis::~Redis()
{
    redisFree(_context);
}

void Redis::connect()
{
    if(_redis_config.address().address() == boost::asio::ip::address())
    {
        throw ska::panda::Error("unable to connect to redis database: invalid address");
    }
    PANDA_LOG << "attempting to connect to redis database: " << _redis_config.address().address().to_string();;
    _context = redisConnect(_redis_config.address().address().to_string().c_str(), _redis_config.address().port());
    if(!_context) {
        throw ska::panda::Error("unable to connect to redis database");
    }
}

RedisReply Redis::query(std::string const& cmd)
{
    if(_context == nullptr)
    {
        connect();
    }
    redisReply* reply = (redisReply *) redisCommand(_context, cmd.c_str());
    if(!reply) {
        PANDA_LOG_ERROR << "Error reading redis database: " << _context->errstr;
        throw ska::panda::Error("error connecting to redis database");
        redisFree(_context);
        connect();
    }
    return RedisReply(reply);
}

} // namespace alfaburst
} // namespace arecibo
