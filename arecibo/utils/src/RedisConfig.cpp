#include "arecibo/utils/RedisConfig.h"


namespace arecibo {
namespace alfaburst {


RedisConfig::RedisConfig()
    : ska::panda::EndpointConfig("redis")
{
    address(ska::panda::IpAddress(6379, "192.231.95.189")); // serendip6 at arecibo
}

RedisConfig::~RedisConfig()
{
}

} // namespace alfaburst
} // namespace arecibo
