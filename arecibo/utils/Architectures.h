#ifndef ARECIBO_ALFABURST_UTILS_ARCHITECTURES_H
#define ARECIBO_ALFABURST_UTILS_ARCHITECTURES_H
#include "panda/Architecture.h"
#include "panda/Cpu.h"
#include "panda/arch/nvidia/Nvidia.h"

namespace arecibo {
namespace alfaburst {

typedef ska::panda::Cpu Cpu;
typedef ska::panda::nvidia::Cuda Cuda;

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_UTILS_ARCHITECTURES_H

