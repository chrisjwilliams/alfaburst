#ifndef SKA_CHEETAH_UTILS_TEST_BINMAPTEST_H
#define SKA_CHEETAH_UTILS_TEST_BINMAPTEST_H

#include <gtest/gtest.h>

namespace arecibo {
namespace alfaburst {
namespace utils {
namespace test {

/**
 * @brief
 * 
 * @details
 * 
 */

class BinMapTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        BinMapTest();

        ~BinMapTest();

    private:
};


} // namespace test
} // namespace utils
} // namespace alfaburst
} // namespace arecibo

#endif // SKA_CHEETAH_UTILS_TEST_BINMAPTEST_H 
