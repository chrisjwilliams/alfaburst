#ifndef ARECIBO_ALFABURST_REDISREPLY_H
#define ARECIBO_ALFABURST_REDISREPLY_H

#include <string>

struct redisReply;

namespace arecibo {
namespace alfaburst {

/**
 * @brief
 *    c++ interface to a Repis query reply
 *
 * @details
 * 
 */
class RedisReply
{
    public:
        RedisReply(redisReply*);
        RedisReply(RedisReply const&) = delete;
        RedisReply(RedisReply&&) = default;
        ~RedisReply();

        /**
         * @brief return true if there has been an error
         */
        bool is_error() const;

        /**
         * @brief return the result/msg of the query
         */
        std::string str() const;

    private:
        redisReply* _reply;
};

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_REDISREPLY_H 
