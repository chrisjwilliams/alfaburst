#ifndef SKA_CHEETAH_DATA_TEST_DATASEQUENCE2DSLOWITERATORTEST_H
#define SKA_CHEETAH_DATA_TEST_DATASEQUENCE2DSLOWITERATORTEST_H

#include <gtest/gtest.h>

namespace arecibo {
namespace alfaburst {
namespace data {
namespace test {

/**
 * @brief
 * 
 * @details
 * 
 */
class DataSequence2DSlowIteratorTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        DataSequence2DSlowIteratorTest();

        ~DataSequence2DSlowIteratorTest();

    private:
};

} // namespace test
} // namespace data
} // namespace alfaburst
} // namespace arecibo

#endif // SKA_CHEETAH_DATA_TEST_DATASEQUENCE2DSLOWITERATORTEST_H 
