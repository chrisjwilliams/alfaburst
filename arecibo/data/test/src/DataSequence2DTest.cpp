#include "arecibo/data/test/DataSequence2DTest.h"
#include "arecibo/data/detail/DataSequence2D.h"
#include "arecibo/data/detail/DataSequence2DFastIterator.h"
#include "arecibo/data/detail/DataSequence2DPartialIterator.h"
#include <stdio.h>


namespace arecibo {
namespace alfaburst {
namespace data {
namespace test {


DataSequence2DTest::DataSequence2DTest()
{
}

DataSequence2DTest::~DataSequence2DTest()
{
}

void DataSequence2DTest::SetUp(){}
void DataSequence2DTest::TearDown(){}

TEST_F(DataSequence2DTest, test_empty)
{
    data::DataSequence2D<Cpu,uint8_t> empty;
    ASSERT_EQ(empty.fast_axis_length(),0U);
    ASSERT_EQ(empty.slow_axis_length(),0U);
}

TEST_F(DataSequence2DTest, test_create)
{
    const std::size_t x=16;
    const std::size_t y=4;
    data::DataSequence2D<Cpu,uint8_t> ds2d(x,y);
    ASSERT_EQ(x,ds2d.fast_axis_length());
    ASSERT_EQ(y,ds2d.slow_axis_length());
}

TEST_F(DataSequence2DTest, test_fill)
{
    const std::size_t x=16;
    const std::size_t y=4;
    data::DataSequence2D<Cpu,uint8_t> ds2d(x,y,144);
    ASSERT_EQ(x,ds2d.fast_axis_length());
    ASSERT_EQ(y,ds2d.slow_axis_length());
    for(auto v : ds2d){
        ASSERT_EQ(uint8_t(144),v);
    }
}


TEST_F(DataSequence2DTest, test_copy)
{
    const std::size_t x=16;
    const std::size_t y=4;
    data::DataSequence2D<Cpu,uint8_t> ds2d(x,y,138);
    ASSERT_EQ(x,ds2d.fast_axis_length());
    ASSERT_EQ(y,ds2d.slow_axis_length());

    data::DataSequence2D<Cpu,uint8_t> ds2d_copy(x,y);
    std::copy(ds2d.begin(), ds2d.end(), ds2d_copy.begin());
    ASSERT_EQ(x,ds2d_copy.fast_axis_length());
    ASSERT_EQ(y,ds2d_copy.slow_axis_length());
    auto it = ds2d.begin();
    auto it2 = ds2d_copy.begin();
    while (it != ds2d.end()){
        ASSERT_EQ(*it,*it2);
        ++it;
        ++it2;
    }
}


TEST_F(DataSequence2DTest, test_partialfill){
    const std::size_t l=4;
    data::DataSequence2D<Cpu,uint8_t> ds2d(l,l,0);

    auto slice = ds2d.slice(1,3,0,2);

    while (slice != slice.end()){
        *slice = 1;
        ++slice;
    }

    int x=0;
    int y=0;
    for (auto v : ds2d){
        if(x >=1 && x < 3 && y >= 0 && y < 2)
        ASSERT_EQ(1u,v);
        else 
        ASSERT_EQ(0u,v);
        ++x;
        if (x == l){
            x=0;
            ++y;
        }

    }
}

#ifndef NDEBUG
TEST_F(DataSequence2DTest, test_copyfailure){
    const std::size_t x=16;
    const std::size_t y=4;
    data::DataSequence2D<Cpu,uint8_t> ds2d(x,y,138);
    ASSERT_EQ(x,ds2d.fast_axis_length());
    ASSERT_EQ(y,ds2d.slow_axis_length());

    data::DataSequence2D<Cpu,uint8_t> ds2d_copy;
    ASSERT_EQ(0U,ds2d_copy.fast_axis_length());
    ASSERT_EQ(0U,ds2d_copy.slow_axis_length());
    ASSERT_DEATH(std::copy(ds2d.begin(), ds2d.end(), ds2d_copy.begin()), "Cannot dereference*");

    data::DataSequence2D<Cpu,uint8_t> ds2d_copy2(x,2);
    ASSERT_DEATH(std::copy(ds2d.begin(), ds2d.end(), ds2d_copy2.begin()),"Cannot dereference*");

}


TEST_F(DataSequence2DTest, test_badslicefailure){
    const std::size_t l=4;
    data::DataSequence2D<Cpu,uint8_t> ds2d(l,l,0);

    ASSERT_DEATH(ds2d.slice(1,6,0,2),"Assert");


}
#endif

} // namespace test
} // namespace data
} // namespace alfaburst
} // namespace arecibo
