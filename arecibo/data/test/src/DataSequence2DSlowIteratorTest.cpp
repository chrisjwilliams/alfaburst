#include "arecibo/data/test/DataSequence2DSlowIteratorTest.h"
#include "arecibo/data/detail/DataSequence2DSlowIterator.h"
#include "arecibo/data/detail/DataSequence2D.h"


namespace arecibo {
namespace alfaburst {
namespace data {
namespace test {


DataSequence2DSlowIteratorTest::DataSequence2DSlowIteratorTest()
    : ::testing::Test()
{
}

DataSequence2DSlowIteratorTest::~DataSequence2DSlowIteratorTest()
{
}

void DataSequence2DSlowIteratorTest::SetUp()
{
}

void DataSequence2DSlowIteratorTest::TearDown()
{
}

TEST_F(DataSequence2DSlowIteratorTest, test_operator_equal)
{
    int val = 99;
    alfaburst::data::DataSequence2D<Cpu, int> data(2, 1, val);
    alfaburst::data::DataSequence2DSlowIterator<Cpu, int> it_1(data);
    alfaburst::data::DataSequence2DSlowIterator<Cpu, int> it_2(data);
    ASSERT_EQ(it_1, it_2++);
    ASSERT_NE(it_1++, it_2);
}

TEST_F(DataSequence2DSlowIteratorTest, test_empty_set)
{
    alfaburst::data::DataSequence2D<Cpu, int> data;
    alfaburst::data::DataSequence2DSlowIterator<Cpu, int> it(data);
    ASSERT_EQ(it, data.end());
}

TEST_F(DataSequence2DSlowIteratorTest, test_deref)
{
    int val = 99;
    alfaburst::data::DataSequence2D<Cpu, int> data(1,1, val);
    alfaburst::data::DataSequence2DSlowIterator<Cpu, int> it(data);
    ASSERT_NE(it, data.end());
    ASSERT_EQ(val, *it);
    ++it;
    ASSERT_EQ(it, data.end());
}

} // namespace test
} // namespace data
} // namespace alfaburst
} // namespace arecibo
