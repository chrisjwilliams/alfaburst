#ifndef SKA_CHEETAH_DATA_DATASEQUENCE2DTEST_H
#define SKA_CHEETAH_DATA_DATASEQUENCE2DTEST_H


#include <gtest/gtest.h>
namespace arecibo {
namespace alfaburst {
namespace data {
namespace test {


/**
 * @brief
 * 
 * @details
 * 
 */
class DataSequence2DTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        DataSequence2DTest();
        ~DataSequence2DTest();

    private:
};


} // namespace test
} // namespace data
} // namespace alfaburst
} // namespace arecibo

#endif // SKA_CHEETAH_DATA_DATASEQUENCE2DTEST_H 
