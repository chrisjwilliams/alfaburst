#ifndef SKA_CHEETAH_DATA_TEST_DATASEQUENCE2DFASTITERATORTEST_H
#define SKA_CHEETAH_DATA_TEST_DATASEQUENCE2DFASTITERATORTEST_H

#include <gtest/gtest.h>

namespace arecibo {
namespace alfaburst {
namespace data {
namespace test {

/**
 * @brief
 * 
 * @details
 * 
 */
class DataSequence2DFastIteratorTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        DataSequence2DFastIteratorTest();

        ~DataSequence2DFastIteratorTest();

    private:
};

} // namespace test
} // namespace data
} // namespace alfaburst
} // namespace arecibo

#endif // SKA_CHEETAH_DATA_TEST_DATASEQUENCE2DFASTITERATORTEST_H 
