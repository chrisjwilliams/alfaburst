#ifndef ARECIBO_ALFABURST_TIMEFREQUENCY_H
#define ARECIBO_ALFABURST_TIMEFREQUENCY_H

//#include "arecibo/data/detail/DataSequence2D.h"
//#include "arecibo/data/Units.h"
//#include "arecibo/utils/chrono.h"
//#include "panda/DataChunk.h"
//#include <boost/units/systems/si/frequency.hpp>
//#include <boost/units/systems/si/time.hpp>
//#include <boost/units/quantity.hpp>
//#include <vector>
//
#include "arecibo/utils/Architectures.h"
#include "cheetah/data/TimeFrequency.h"

namespace arecibo {
namespace alfaburst {
namespace data {

using pss::astrotypes::units::Frequency;
using pss::astrotypes::units::Time;
template<typename T> using DimensionSize = pss::astrotypes::DimensionSize<T>;
template<typename T> using DimensionIndex = pss::astrotypes::DimensionIndex<T>;

template<typename Arch, typename NumericalT=uint16_t> using TimeFrequency = ska::cheetah::data::TimeFrequency<Arch, NumericalT>;

} // namespace data
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_TIMEFREQUENCY_H 
