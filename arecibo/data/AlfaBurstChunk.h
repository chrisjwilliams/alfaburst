#ifndef ARECIBO_ALPHABURST_ALPHABURSTCHUNK_H
#define ARECIBO_ALPHABURST_ALPHABURSTCHUNK_H

#include <vector>

namespace arecibo {
namespace alfaburst {

/**
 * @class AlfaBurstChunk
 * 
 * @brief
 *     Arecibo Data Stream Chunk formatted for the AlfaBurst pipeline
 */
class AlfaBurstChunk
{
    public:
        typedef float T;

    public:
        AlfaBurstChunk();
        ~AlfaBurstChunk();

        /// Returns the number of blocks of sub-band spectra.
        inline unsigned nTimeBlocks() const { return _nTimeBlocks; }

        /// Returns the number of sub-bands in the data.
        inline unsigned nSubbands() const { return _nSubbands; }

        /// Returns the number of polarisations in the data.
        inline unsigned nPolarisations() const { return _nPolarisations; }

        /// Returns the number of polarisations in the data.
        inline unsigned nPolarisationComponents() const { 
            return _nPolarisations; 
        }

        /// Return the number of channels for the spectrum specified by index @p i
        inline unsigned nChannels() const { return _nChannels; }

        /// Return the time (in seconds) of the corresponding timeSlice
        double get_time( unsigned sample_number ) const { 
            return _timestamp + sample_number * _block_rate;
        }

        /// Return the block rate (time-span of the each timeslice)
        double BlockRate() const { return _block_rate; }

        /// Set the block rate (time-span of each timeslice)
        void BlockRate(double block_rate) { _block_rate = block_rate; }

        /// Return the timestamp
        inline double get_timestamp() const { return _timestamp; }

        /// Set the time-stamp
        void set_timestamp(double timestamp) { _timestamp = timestamp; }

        /// calculates what the index should be given the block, subband, polarisation (primarily used as an aid to optimisation).
        static inline long index(unsigned subband, unsigned numSubbands,
                   unsigned polarisation, unsigned numPolarisations,
                   unsigned block, unsigned numChannels);

        /// Returns a pointer to the spectrum data for the specified time block @p b, sub-band @p s, and polarisation @p p (const overload).
        T* spectrumData(unsigned b, unsigned s, unsigned p);

        /// resize the data set to the specified dimensions.
        void resize(unsigned nTimeBlocks, unsigned nSubbands,
                unsigned nPolarisations, unsigned nChannels);

        /// Return the overall size of the data
        int size() const;

        /// Returns a pointer to the data.
        T * data() { return &_data[0]; }

        /// Returns a pointer to the data (const overload).
        T const * data() const { return &_data[0]; }

        bool operator==(const AlfaBurstChunk& data) const;

        void invalidate();
        bool is_valid() const;

    private:
        /// Returns the data index for a given time block @p b, sub-band @p s and polarisation @p p
        unsigned long _index(unsigned s, unsigned p, unsigned b) const;

    protected:
        unsigned _nSubbands;
        unsigned _nPolarisations;
        unsigned _nTimeBlocks;
        unsigned _nChannels;

        std::vector<T> _data;

        double  _block_rate;
        double  _timestamp;

        bool _valid; // TODO move to base class
};

inline long AlfaBurstChunk::index( unsigned subband, unsigned numSubbands,
        unsigned polarisation, unsigned numPolarisations, unsigned block,
        unsigned numChannels)
{
    return numChannels * ( numPolarisations * ( numSubbands * block + subband )
            + polarisation );
}

inline
unsigned long AlfaBurstChunk::_index(unsigned s, unsigned p, unsigned b) const
{
    return index(s, _nSubbands, p, _nPolarisations, b, _nChannels );
}


} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALPHABURST_ALPHABURSTCHUNK_H 
