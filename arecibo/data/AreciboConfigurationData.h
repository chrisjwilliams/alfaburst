#ifndef ARECIBO_ALFABURST_ARECIBOCONFIGURATIONDATA_H
#define ARECIBO_ALFABURST_ARECIBOCONFIGURATIONDATA_H


namespace arecibo {
namespace alfaburst {
namespace data {

/**
 * @class AreciboConfigurationData
 * 
 * @brief
 * 
 * @details
 * 
 */
class AreciboConfigurationData
{
    public:
        AreciboConfigurationData();

        ~AreciboConfigurationData();

    private:
};

} // namespace data
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_ARECIBOCONFIGURATIONDATA_H 
