#ifndef ARECIBO_ALFABURST_DATA_UNITS_H
#define ARECIBO_ALFABURST_DATA_UNITS_H

#include "cheetah/data/Units.h"

namespace arecibo {
namespace alfaburst {
namespace data {
    typedef typename boost::units::make_scaled_unit<boost::units::si::frequency, boost::units::scale<10, boost::units::static_rational<6> > >::type MegaHertz;
    typedef boost::units::quantity<MegaHertz, double> FrequencyType;

} // namespace data
} // namespace alfaburst
} // namespace arecibo

// add hashing function for boost::units::quantity types
/*
namespace std {
    template<typename Unit, typename ValueType>
    struct hash<boost::units::quantity<Unit,ValueType>> {
        std::size_t operator()(boost::units::quantity<Unit, ValueType> const& q) const
        {
            return std::hash<ValueType>()(q.value());
        }
    };
}
*/
#endif // ARECIBO_ALFABURST_DATA_UNITS_H 
