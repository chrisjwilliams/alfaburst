#ifndef ARECIBO_ALFABURST_DATA_DATASEQUENCE2DFASTITERATOR_H
#define ARECIBO_ALFABURST_DATA_DATASEQUENCE2DFASTITERATOR_H

#include <iterator>
#include <memory>

#include "arecibo/data/detail/DataSequence2DIterator.h"
#include "arecibo/utils/Architectures.h"

namespace arecibo {
namespace alfaburst {
namespace data {

/**
 * @brief Iterator over DataSequence2D types, over "fast" axis. Generic type.
 * 
 */
template <typename Arch, typename Type>
class DataSequence2DFastIterator : public DataSequence2DIterator<Arch,Type>{
};

template<typename Arch, typename T>
class DataSequence2D;

/**
 * @brief Iterator over DataSequence2D types, over "fast" axis. CPU specific type.
 * 
 * @tparam Type The datatype stored in the DataSequence2D.
 */
template <typename Type>
class DataSequence2DFastIterator<Cpu,Type> : public std::iterator<std::random_access_iterator_tag, Type,std::size_t,Type*,Type&>
{
    public:
        typedef typename std::iterator_traits<DataSequence2DFastIterator<Cpu,Type>>::pointer PtrType;
        typedef typename std::iterator_traits<DataSequence2DFastIterator<Cpu,Type>>::reference RefType;
        typedef typename std::iterator_traits<DataSequence2DFastIterator<Cpu,Type>>::difference_type DiffType;
        typedef DataSequence2DFastIterator<Cpu,Type> SelfType;

    public:
        /**
         * @brief Create an iterator
         *
         * @details Iterator starts at the top of the "slow" axis given by
         * slowOffset.
         * @arg data_ptr the data to iterate over
         * @arg slowOffset the slow axis offset, defaults to zero.
         */
        explicit DataSequence2DFastIterator(const DataSequence2D<Cpu,typename std::remove_cv<Type>::type> &data_ptr,
                std::size_t slow_offset=0);

        explicit DataSequence2DFastIterator(DataSequence2D<Cpu,typename std::remove_cv<Type>::type> &data_ptr,
                std::size_t slow_offset=0);

        /**
         * @brief default constructor. Required by random_access_iterator_tag
         */
        DataSequence2DFastIterator();

        /**
         * @brief copy constructor. Required by random_access_iterator_tag.
         */
        DataSequence2DFastIterator(const DataSequence2DFastIterator<Cpu,Type> &copy);

        /**
         * @brief Destroy the iterator
         */
        ~DataSequence2DFastIterator();


        /**
         * @brief dereference operators
         * @details return the actual data currently referenced by the iterator
         */
        virtual PtrType operator->() const;

        /**
         * @brief dereference operators
         */
        virtual RefType operator*() const;


        /**
         * @brief pre-increment operator. 
         * @returns the iterator now pointing to the next element in the data sequence
         * @details at the end of the sequence will contain nullptr. 
         *          Unlike the pot-increment operator, no copy is made.
         */
        SelfType& operator++();

        /**
         * @brief post-increment the iterator to the next element in the data sequence
         * @returns A copy of the iterator in its pre-incremented state.
         */
        SelfType operator++(int);

        /**
         * @brief Decriment
         */
        SelfType& operator--();

        /**
         * @brief Decriment
         */
        SelfType operator--(int);

        /**
         * @brief Add-Asignment
         */
        SelfType& operator+=(DiffType off);

        /**
         * @brief Subtract-asignment
         */
        SelfType& operator-=(DiffType off);

        /**
         * Add offset
         */
        SelfType operator+(DiffType off) const;

        /**
         * Subtract offset
         */
        SelfType operator-(DiffType off) const;

        /**
         * Subtract iterator
         */
        DiffType operator-(const SelfType &right) const;

        /**
         * less than
         */
        bool operator<(const SelfType &right) const;

        /**
         * Greater Than
         */
        bool operator>(const SelfType &right) const;

        /**
         * Less than or Equal.
         */
        bool operator<=(const SelfType &right) const;

        /**
         * Greater or Equal
         */
        bool operator>=(const SelfType &right) const;

        /**
         * @brief Offset dereference
         */
        RefType operator[](DiffType off) const;

        template<typename T>
        friend bool operator==(DataSequence2DFastIterator<Cpu,T> const& it1, DataSequence2DFastIterator<Cpu,T> const& it2); 
        template<typename T>
        friend bool operator!=(DataSequence2DFastIterator<Cpu,T> const& it1, DataSequence2DFastIterator<Cpu,T> const& it2); 

    protected:
        std::size_t index();
        const DataSequence2D<Cpu,typename std::remove_cv<Type>::type> *_data;
        std::size_t _offset=0;
        Type* _raw;

};

} // namespace data
} // namespace alfaburst
} // namespace arecibo

#include "arecibo/data/detail/DataSequence2DFastIterator.cpp"

#endif // ARECIBO_ALFABURST_DATA_DATASEQUENCE2DFASTITERATOR_H 
