#ifndef ARECIBO_ALFABURST_DATA_DATASEQUENCE2DITERATOR_H
#define ARECIBO_ALFABURST_DATA_DATASEQUENCE2DITERATOR_H

namespace arecibo {
namespace alfaburst {
namespace data {

/**
 * @brief A generic iterator for DataSequence2D.
 *
 * @details Do not instantiate this, use a specialisation!
 */
template <typename Arch, typename Type>
class DataSequence2DIterator
{
    protected:
        DataSequence2DIterator();
        ~DataSequence2DIterator();
};

} // namespace data
} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_DATA_DATASEQUENCE2DITERATOR_H 
