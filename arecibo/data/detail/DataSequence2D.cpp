namespace arecibo {
namespace alfaburst {
namespace data {

template <typename T>
DataSequence2D<Cpu,T>::DataSequence2D(){}

template <typename T>
DataSequence2D<Cpu,T>::DataSequence2D(std::size_t fast_axis_length, std::size_t slow_axis_length)
    : _fast_axis_length(fast_axis_length)
    , _slow_axis_length(slow_axis_length)
    , _data()
{
    this->_data.resize(fast_axis_length*slow_axis_length);
}

template <typename T>
DataSequence2D<Cpu,T>::DataSequence2D(std::size_t fast_axis_length, std::size_t slow_axis_length, const T &fill)
    : _fast_axis_length(fast_axis_length)
    , _slow_axis_length(slow_axis_length)
    , _data(fast_axis_length*slow_axis_length,fill)
{
}

template <typename T>
DataSequence2D<Cpu,T>::~DataSequence2D(){}

template <typename T>
std::size_t DataSequence2D<Cpu,T>::fast_axis_length() const { return this->_fast_axis_length;}

template <typename T>
std::size_t DataSequence2D<Cpu,T>::slow_axis_length() const { return this->_slow_axis_length;}

template <typename T>
void DataSequence2D<Cpu,T>::resize(std::size_t fast_axis_length, std::size_t slow_axis_length, const T &fill)
{
    this->_fast_axis_length = fast_axis_length;
    this->_slow_axis_length = slow_axis_length;
    this->_data.resize(this->_fast_axis_length*this->_slow_axis_length);
    std::fill(this->_data.begin(),this->_data.end(), fill);
}

template <typename T>
void DataSequence2D<Cpu,T>::resize(std::size_t fast_axis_length, std::size_t slow_axis_length)
{
    this->_fast_axis_length = fast_axis_length;
    this->_slow_axis_length = slow_axis_length;
    this->_data.resize(this->_fast_axis_length*this->_slow_axis_length);
}

template <typename T>
T const* DataSequence2D<Cpu,T>::data() const {return &(this->_data[0]);}

template <typename T>
T* DataSequence2D<Cpu,T>::data() {return &(this->_data[0]);}

template <typename T>
typename DataSequence2D<Cpu,T>::Iterator DataSequence2D<Cpu,T>::begin() {
    return Iterator(*this);
}

template <typename T>
typename DataSequence2D<Cpu,T>::ConstIterator DataSequence2D<Cpu,T>::begin() const {
    return ConstIterator(*this);
}

template <typename T>
typename DataSequence2D<Cpu,T>::ConstIterator DataSequence2D<Cpu,T>::cbegin() const {
    return ConstIterator(*this);
}


template <typename T>
typename DataSequence2D<Cpu,T>::Iterator DataSequence2D<Cpu,T>::end() {
    return Iterator(*this)+this->_data.size();
}

template <typename T>
typename DataSequence2D<Cpu,T>::ConstIterator DataSequence2D<Cpu,T>::end() const {
    return ConstIterator(*this)+this->_data.size();
}

template <typename T>
typename DataSequence2D<Cpu,T>::ConstIterator DataSequence2D<Cpu,T>::cend() const {
    return ConstIterator(*this)+this->_data.size();
}

template <typename T>
typename DataSequence2D<Cpu,T>::PartialIterator DataSequence2D<Cpu,T>::slice(std::size_t fbegin,
        std::size_t fend,
        std::size_t sbegin,
        std::size_t send){

    BOOST_ASSERT(fbegin >= 0);
    BOOST_ASSERT(fend < this->fast_axis_length());
    BOOST_ASSERT(sbegin >= 0);
    BOOST_ASSERT(send < this->slow_axis_length());

    return DataSequence2DPartialIterator<Cpu,T>(*this, fbegin,fend,sbegin,send);
}

} // namespace data
} // namespace alfaburst
} // namespace arecibo



