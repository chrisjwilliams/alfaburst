#include "arecibo/data/detail/DataSequence2D.h"
#include <boost/assert.hpp>

namespace arecibo {
namespace alfaburst {
namespace data {

template<typename Type>
DataSequence2DSlowIterator<Cpu,Type>::~DataSequence2DSlowIterator()
{
}

template<typename Type>
DataSequence2DSlowIterator<Cpu,Type>::DataSequence2DSlowIterator(
        DataSequence2D<Cpu,typename std::remove_cv<Type>::type> &data,
        std::size_t slow_offset
        )
    : _data(data)
    , _pos(0)
{
    this->_offset = slow_offset*data.fast_axis_length();
}

template<typename Type>
DataSequence2DSlowIterator<Cpu,Type>::DataSequence2DSlowIterator(
        const DataSequence2D<Cpu,typename std::remove_cv<Type>::type> &data,
        std::size_t slow_offset
        )
    : _data(data)
    , _pos(0)
{
    this->_offset = slow_offset*data.fast_axis_length();
}

template<typename Type>
typename DataSequence2DSlowIterator<Cpu, Type>::RefType DataSequence2DSlowIterator<Cpu,Type>::dereference() const 
{
    return *(_data.data() + _offset + _pos);
}

template<typename Type>
void DataSequence2DSlowIterator<Cpu,Type>::decrement() 
{
    if(_pos == 0 ) {
        _offset -= _data.fast_axis_length();
        _pos = _data.fast_axis_length() - 1;
    }
    else {
        --_pos;
    }
}

template<typename Type>
void DataSequence2DSlowIterator<Cpu,Type>::advance(std::size_t n) 
{
    auto col_size = _data.fast_axis_length();
    std::size_t tot_column = (n/col_size) * col_size;
    _offset += tot_column;
    _pos += n - tot_column;
    if( _pos >= col_size ) {
        _offset += col_size;
        _pos -= col_size; 
    }
}

template<typename Type>
void DataSequence2DSlowIterator<Cpu,Type>::increment() 
{
    if(++_pos >= _data.fast_axis_length()) {
        _offset += _data.fast_axis_length();
        _pos = 0;
    }
}

template<typename Type>
bool DataSequence2DSlowIterator<Cpu,Type>::equal(const SelfType& right) const {
    return (&(this->_data)==&(right._data)) && ((_offset + _pos) == (right._offset + right._pos));
}

template<typename Type>
typename DataSequence2DSlowIterator<Cpu, Type>::DiffType DataSequence2DSlowIterator<Cpu,Type>::distance_to(SelfType const& right) const
{
    return right._offset + right._pos - _offset + _pos;
}

} // namespace data
} // namespace alfaburst
} // namespace arecibo

