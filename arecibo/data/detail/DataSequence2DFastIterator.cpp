#include <boost/assert.hpp>

namespace arecibo {
namespace alfaburst {
namespace data {

template<typename Type>
DataSequence2DFastIterator<Cpu,Type>::~DataSequence2DFastIterator()
{
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type>::DataSequence2DFastIterator()
    :_data(nullptr)
{
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type>::DataSequence2DFastIterator(const DataSequence2DFastIterator &copy) 
    :_data(copy._data)
    ,_offset(copy._offset)
    ,_raw(copy._raw)
{
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type>::DataSequence2DFastIterator(
        DataSequence2D<Cpu,typename std::remove_cv<Type>::type> &data_ptr,
        std::size_t slow_offset
        )
{
    this->_data = &data_ptr;
    this->_raw = data_ptr.data();
    this->_offset = slow_offset*data_ptr.fast_axis_length();
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type>::DataSequence2DFastIterator(
        const DataSequence2D<Cpu,typename std::remove_cv<Type>::type> &data_ptr,
        std::size_t slow_offset
        )
{
    this->_data = &data_ptr;
    this->_raw = data_ptr.data();
    this->_offset = slow_offset*data_ptr.fast_axis_length();
}



template<typename Type>
typename DataSequence2DFastIterator<Cpu,Type>::PtrType  DataSequence2DFastIterator<Cpu,Type>::operator->() const{
    BOOST_ASSERT_MSG(this->_raw != nullptr,"Cannot dereference iterator over null DataSequence2D");
    BOOST_ASSERT_MSG(this->_offset < this->_data->fast_axis_length()*this->_data->slow_axis_length(),"Cannot dereference post-end iterator");
    return this->_raw+this->_offset;
}

template<typename Type>
typename DataSequence2DFastIterator<Cpu,Type>::RefType  DataSequence2DFastIterator<Cpu,Type>::operator*() const{
    BOOST_ASSERT_MSG(this->_raw != nullptr,"Cannot dereference iterator over null DataSequence2D");
    BOOST_ASSERT_MSG(this->_offset < this->_data->fast_axis_length()*this->_data->slow_axis_length(),"Cannot dereference post-end iterator");
    return *(this->_raw+this->_offset);
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type>&  DataSequence2DFastIterator<Cpu,Type>::operator++(){
    ++(this->_offset);
    return *this;
}


template<typename Type>
DataSequence2DFastIterator<Cpu,Type>  DataSequence2DFastIterator<Cpu,Type>::operator++(int){
    DataSequence2DFastIterator<Cpu,Type> r(*this);
    ++(this->_offset);
    return r;
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type>&  DataSequence2DFastIterator<Cpu,Type>::operator--(){
    --(this->_offset);
    return *this;
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type>  DataSequence2DFastIterator<Cpu,Type>::operator--(int){
    DataSequence2DFastIterator<Cpu,Type> r(*this);
    --(this->_offset);
    return r;
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type>&  DataSequence2DFastIterator<Cpu,Type>::operator+=(typename DataSequence2DFastIterator<Cpu,Type>::DiffType off){
    this->_offset += off;
    return *this;
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type>& DataSequence2DFastIterator<Cpu,Type>::operator-=(typename DataSequence2DFastIterator<Cpu,Type>::DiffType off){
    this->_offset -= off;
    return *this;
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type> DataSequence2DFastIterator<Cpu,Type>::operator+(typename DataSequence2DFastIterator<Cpu,Type>::DiffType off) const{
    auto r(*this);
    r._offset += off;
    return r;
}

template<typename Type>
DataSequence2DFastIterator<Cpu,Type> DataSequence2DFastIterator<Cpu,Type>::operator-(typename DataSequence2DFastIterator<Cpu,Type>::DiffType off) const{
    auto r(*this);
    r._offset -= off;
    return r;
}

template<typename Type>
typename DataSequence2DFastIterator<Cpu,Type>::DiffType DataSequence2DFastIterator<Cpu,Type>::operator-(const DataSequence2DFastIterator<Cpu,Type> &right) const{
    return this->_offset - right._offset;
}

template<typename Type>
bool DataSequence2DFastIterator<Cpu,Type>::operator<(const DataSequence2DFastIterator<Cpu,Type> &right) const {
    return this->_offset < right._offset;
}

template<typename Type>
bool DataSequence2DFastIterator<Cpu,Type>::operator>(const DataSequence2DFastIterator<Cpu,Type> &right) const{
    return this->_offset > right._offset;
}

template<typename Type>
bool DataSequence2DFastIterator<Cpu,Type>::operator<=(const DataSequence2DFastIterator<Cpu,Type> &right) const{
    return this->_offset <= right._offset;
}

template<typename Type>
bool DataSequence2DFastIterator<Cpu,Type>::operator>=(const DataSequence2DFastIterator<Cpu,Type> &right) const {
    return (this->_data==right._data) && (this->_offset >= right._offset);
}

template<typename Type>
typename DataSequence2DFastIterator<Cpu,Type>::RefType DataSequence2DFastIterator<Cpu,Type>::operator[](typename DataSequence2DFastIterator<Cpu,Type>::DiffType off) const {
    return *((*this)+off);
}

template<typename Type>
bool operator==(DataSequence2DFastIterator<Cpu,Type> const& it1, DataSequence2DFastIterator<Cpu,Type> const& it2){
    return (it1._data == it2._data) && (it1._offset == it2._offset);
}

template<typename Type>
bool operator!=(DataSequence2DFastIterator<Cpu,Type> const& it1, DataSequence2DFastIterator<Cpu,Type> const& it2){
    return (it1._data == it2._data) && (it1._offset != it2._offset);
}


} // namespace data
} // namespace alfaburst
} // namespace arecibo

