#include "arecibo/data/AlfaBurstChunk.h"

/**
 * @brief Container class to hold a buffer of blocks of spectra ordered by time, sub-band and polarisation.
 */

namespace arecibo {
namespace alfaburst {

AlfaBurstChunk::AlfaBurstChunk()
    : _valid(true)
{
}

AlfaBurstChunk::~AlfaBurstChunk()
{
}

void AlfaBurstChunk::resize(unsigned nTimeBlocks, unsigned nSubbands,
        unsigned nPolarisations, unsigned nChannels)
{
    _nSubbands      = nSubbands;
    _nPolarisations = nPolarisations;
    _nTimeBlocks    = nTimeBlocks;
    _nChannels      = nChannels;

    _valid = true;
    _data.resize(nSubbands * nPolarisations * nTimeBlocks * nChannels);
}

int AlfaBurstChunk::size() const
{
    return _data.size();
}

bool AlfaBurstChunk::operator==( const AlfaBurstChunk& data ) const
{
    if( data._nTimeBlocks == _nTimeBlocks 
        && data._nPolarisations == _nPolarisations
        && data._nSubbands == _nSubbands
        && data._nChannels == _nChannels )
    {
        // check data contents
        for( unsigned i=0; i < _data.size(); ++i ) {
            if( _data[i] != data._data[i] ) {
                return false;
            }
        }
        return true;
    }
    return false;
}

AlfaBurstChunk::T * AlfaBurstChunk::spectrumData(unsigned b, unsigned s, unsigned p)
{ 
    return &_data[_index(s, p, b)]; 
}

bool AlfaBurstChunk::is_valid() const
{
    return _valid;
}

void AlfaBurstChunk::invalidate() 
{
    _valid=false;
}

} // namespace alfaburst
} // namespace arecibo
