#ifndef ARECIBO_ALPHABURST_EMULATOR_H
#define ARECIBO_ALPHABURST_EMULATOR_H

#include "arecibo/receptor/AlfaBurstPacketGenerator.h"
#include "cheetah/generators/TimeFrequencyGenerator.h"
#include "cheetah/emulator/Emulator.h"

namespace arecibo {
namespace alfaburst {

/**
 * @brief
 *    Packs data into the arecibo UDP packet format
 */
class Emulator : public ska::cheetah::emulator::Emulator<receptor::AlfaBurstPacketGenerator, uint16_t>
{
        typedef ska::cheetah::emulator::Emulator<receptor::AlfaBurstPacketGenerator, uint16_t> BaseT;

    public:
        typedef BaseT::ModelType ModelType;

    public:
        Emulator(ska::cheetah::emulator::Config const&, ModelType* model);
};

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALPHABURST_EMULATOR_H 
