#ifndef ARECIBO_ALFABURST_EMULATORRUNNER_H
#define ARECIBO_ALFABURST_EMULATORRUNNER_H

#include "arecibo/emulator/Emulator.h"
#include "cheetah/emulator/Config.h"
#include <thread>


namespace arecibo {
namespace alfaburst {

/**
 * @brief
 *   Convenience class to own and maintain an Emulator in its own thread
 * @details 
 *   Emulator will be start on construction
 *   Emulator will be stopped on destruction
 */
class EmulatorRunner
{
        typedef ska::cheetah::emulator::Config Config;
        typedef Emulator::ModelType DataModel;

    public:
        EmulatorRunner(Config const& s, DataModel*);
        ~EmulatorRunner();

        /**
         * @brief block until emulator is in a running state
         */
        void wait_run() const;

        /**
         * @return the emualtor that is being managed
         */
        Emulator const& emulator() const;

    private:
        Emulator _emulator;
        std::thread _t;
        bool _finished;
};

} // namespace alfaburst
} // namespace arecibo

#endif // ARECIBO_ALFABURST_EMULATORRUNNER_H 
