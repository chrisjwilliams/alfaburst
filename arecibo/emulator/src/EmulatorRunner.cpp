#include "arecibo/emulator/EmulatorRunner.h"
#include <iostream>


namespace arecibo {
namespace alfaburst {

EmulatorRunner::EmulatorRunner(Config const& s, DataModel* model)
    : _emulator(s, model)
    , _t( [&]() { try {
                            _emulator.run(); 
                           } catch( std::exception const& e ) {
                             std::cerr << "exception thrown during emulator::run()" << std::endl;
                           }
                         } )
    , _finished(false)
{
}

EmulatorRunner::~EmulatorRunner()
{
    _finished = true;
    _emulator.stop();
    do { std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while(_emulator.is_running());
    _t.join();
}

void EmulatorRunner::wait_run() const
{
    if(!_finished) {
        do { std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while(!_emulator.is_running());
    }
}

Emulator const& EmulatorRunner::emulator() const
{
    return _emulator;
}

} // namespace alfaburst
} // namespace arecibo
