#include "arecibo/emulator/Emulator.h"

namespace arecibo {
namespace alfaburst {

Emulator::Emulator(ska::cheetah::emulator::Config const& config, ModelType* model)
    : BaseT(config, model)
{
}

} // namespace alfaburst
} // namespace arecibo
