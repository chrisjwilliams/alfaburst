/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Oxford Univeristy Astrophysics
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef ARECIBO_ALFABURST_PIPELINE_LAUNCHER_H
#define ARECIBO_ALFABURST_PIPELINE_LAUNCHER_H

#include "Factory.h"
#include "panda/Launcher.h"
#include "arecibo/sink/Config.h"
#include "arecibo/source/Factory.h"
#include "arecibo/source/Config.h"

namespace arecibo {
namespace alfaburst {
namespace pipeline {

template<typename NumericalT>
struct AlfaburstLauncherTraits {
    typedef sink::Factory<NumericalT> Sink;
    typedef sink::Config SinkConfig;
    typedef source::Config SourceConfig;
    typedef Factory<NumericalT> ComputeFactory;
    typedef alfaburst::AlfaBurstConfiguration Config;
    typedef source::Factory<NumericalT> SourceFactory;
};

/**
 * @brief
 *    template class for launcher
 * @details
 */

template<typename NumericalT>
class Launcher : public ska::panda::Launcher<AlfaburstLauncherTraits<NumericalT>>
{
    public:
        Launcher();
        ~Launcher();

    private:
};


} // namespace pipeline
} // namespace alfaburst
} // namespace arecibo
#include "detail/Launcher.cpp"

#endif // ARECIBO_ALFABURST_PIPELINE_LAUNCHER_H
