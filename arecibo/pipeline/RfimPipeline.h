#ifndef ARECIBO_ALFABURST_PIPELINE_RFIMPIPELINE_H
#define ARECIBO_ALFABURST_PIPELINE_RFIMPIPELINE_H

#include "ProcessingPipeline.h"
#include "cheetah/rfim/Rfim.h"

namespace arecibo {
namespace alfaburst {
namespace pipeline {

/**
 * @brief
 *    Template class for an Rfim pipeline
 * @details
 *
 */
template<typename NumericalT, typename RfiPolicy=ska::cheetah::rfim::FlagPolicy<typename ProcessingPipeline<NumericalT>::TimeFrequencyType>>
class RfimPipeline : public ProcessingPipeline<NumericalT>
{
        typedef ProcessingPipeline<NumericalT> BaseT;

    protected:
        typedef typename BaseT::TimeFrequencyType TimeFrequencyType;

    public:
        RfimPipeline(AlfaBurstConfiguration const&, typename BaseT::Exporter&);
        RfimPipeline(RfimPipeline&&);
        ~RfimPipeline();

        void operator()(TimeFrequencyType&, data::AreciboConfigurationData&) override;

    private:
        class RfiOutputHandler {
            public:
                RfiOutputHandler(RfimPipeline&);
                template<typename RfiDataType>
                void operator()(RfiDataType const&) const;

            private:
                RfimPipeline& _pipeline;
        };

        class BandPassOutputHandler {
                typedef ska::cheetah::rfim::ampp::Spectrum<NumericalT> BandPassType;

            public:
                BandPassOutputHandler(RfimPipeline&);
                inline void operator()(BandPassType const&);

            private:
                RfimPipeline& _pipeline;
        };

        struct RfimTraits {
            typedef RfiOutputHandler RfimHandler;
            typedef BandPassOutputHandler BandPassHandler;
            typedef RfiPolicy Policy;
            typedef NumericalT NumericalRep;
        };

        typedef ska::cheetah::rfim::Rfim<TimeFrequencyType, RfimTraits, ska::cheetah::rfim::ConfigType<AlfaBurstConfiguration::PoolManagerType>> RfimType;

    private:
        RfiOutputHandler _rfim_handler;
        BandPassOutputHandler _bandpass_handler;
        RfimType _rfim;
};

} // namespace pipeline
} // namespace alfaburst
} // namespace arecibo
#include "detail/RfimPipeline.cpp"

#endif // ARECIBO_ALFABURST_PIPELINE_RFIMPIPELINE_H
