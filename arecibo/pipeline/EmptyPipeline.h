#ifndef ARECIBO_ALFABURST_PIPELINE_EMPTYPIPELINE_H
#define ARECIBO_ALFABURST_PIPELINE_EMPTYPIPELINE_H


#include "ProcessingPipeline.h"

namespace arecibo {
namespace alfaburst {
namespace pipeline {

/**
 * @brief
 *   A noop pipeline 
 */
template<typename NumericalT>
class EmptyPipeline : public ProcessingPipeline<NumericalT>
{
        typedef ProcessingPipeline<NumericalT> BaseT;
        typedef typename BaseT::TimeFrequencyType TimeFrequencyType;

    public:
        EmptyPipeline(AlfaBurstConfiguration const&, typename BaseT::Exporter&);
        ~EmptyPipeline();

        void operator()(TimeFrequencyType&, data::AreciboConfigurationData&) override;

    private:
};

} // namespace pipeline
} // namespace alfaburst
} // namespace arecibo
#include "detail/EmptyPipeline.cpp"

#endif // ARECIBO_ALFABURST_PIPELINE_EMPTYPIPELINE_H 
