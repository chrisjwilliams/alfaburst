#include "arecibo/pipeline/EmptyPipeline.h"
#include "arecibo/AlfaBurstConfiguration.h"


namespace arecibo {
namespace alfaburst {
namespace pipeline {

template<typename NumericalT>
EmptyPipeline<NumericalT>::EmptyPipeline(AlfaBurstConfiguration const&, typename BaseT::Exporter& exporter)
    : BaseT(exporter)
{
}

template<typename NumericalT>
EmptyPipeline<NumericalT>::~EmptyPipeline()
{
}

template<typename NumericalT>
void EmptyPipeline<NumericalT>::operator()(TimeFrequencyType& data, data::AreciboConfigurationData&)
{
    this->out().send(ska::panda::ChannelId("raw"), data);
}

} // namespace pipeline
} // namespace alfaburst
} // namespace arecibo
