
namespace arecibo {
namespace alfaburst {
namespace pipeline {


template<typename NumericalT, typename RfiPolicy>
RfimPipeline<NumericalT, RfiPolicy>::RfimPipeline(AlfaBurstConfiguration const& config, typename BaseT::Exporter& exporter)
    : BaseT(exporter)
    , _rfim_handler(*this)
    , _bandpass_handler(*this)
    , _rfim(config.module_config<ska::cheetah::rfim::Config>(), _rfim_handler, _bandpass_handler)
{
}

template<typename NumericalT, typename RfiPolicy>
RfimPipeline<NumericalT, RfiPolicy>::RfimPipeline(RfimPipeline&& rp)
    : _rfim(std::move(rp._rfim))
{
}

template<typename NumericalT, typename RfiPolicy>
RfimPipeline<NumericalT, RfiPolicy>::~RfimPipeline()
{
}

template<typename NumericalT, typename RfiPolicy>
void RfimPipeline<NumericalT, RfiPolicy>::operator()(TimeFrequencyType& data, data::AreciboConfigurationData&)
{
    _rfim.run(data);
    this->out().send(ska::panda::ChannelId("rfim"), data);
}

template<typename NumericalT, typename RfiPolicy>
RfimPipeline<NumericalT, RfiPolicy>::RfiOutputHandler::RfiOutputHandler(RfimPipeline<NumericalT, RfiPolicy>& p)
    : _pipeline(p)
{
}

template<typename NumericalT, typename RfiPolicy>
template<typename RfiDataType>
void RfimPipeline<NumericalT, RfiPolicy>::RfiOutputHandler::operator()(RfiDataType const& data) const
{
    TimeFrequencyType& tf_data = static_cast<TimeFrequencyType&>(ska::panda::is_pointer_wrapper<typename std::remove_reference<RfiDataType>::type>::extract(data));
    _pipeline.out().send(ska::panda::ChannelId("rfim"), tf_data);
}

template<typename NumericalT, typename RfiPolicy>
RfimPipeline<NumericalT, RfiPolicy>::BandPassOutputHandler::BandPassOutputHandler(RfimPipeline<NumericalT, RfiPolicy>& p)
    : _pipeline(p)
{
}

template<typename NumericalT, typename RfiPolicy>
void RfimPipeline<NumericalT, RfiPolicy>::BandPassOutputHandler::operator()(BandPassType const& bandpass)
{
    _pipeline.out().send(ska::panda::ChannelId("bandpass"), bandpass);
}

} // namespace pipelins
} // namespace alfaburst
} // namespace arecibo
