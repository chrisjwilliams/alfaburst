#include "arecibo/pipeline/ProcessingPipeline.h"


namespace arecibo {
namespace alfaburst {
namespace pipeline {


template<typename NumericalT>
ProcessingPipeline<NumericalT>::ProcessingPipeline(Exporter& exporter)
    : _out(exporter)
{
}

template<typename NumericalT>
ProcessingPipeline<NumericalT>::~ProcessingPipeline()
{
}

} // namespace pipeline
} // namespace alfaburst
} // namespace arecibo
