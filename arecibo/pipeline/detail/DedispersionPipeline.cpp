
namespace arecibo {
namespace alfaburst {
namespace pipeline {


template<typename NumericalT>
DedispersionPipeline<NumericalT>::DedispersionPipeline(AlfaBurstConfiguration const& config, typename BaseT::Exporter& exporter)
    : BaseT(config, exporter)
    , _dedispersion_handler(*this)
    , _sps_handler(*this)
    , _sps(config.module_config<ska::cheetah::sps::Config>(), _dedispersion_handler, _sps_handler)
{
}

template<typename NumericalT>
DedispersionPipeline<NumericalT>::DedispersionPipeline(DedispersionPipeline&& rp)
    : _sps(std::move(rp._sps))
{
}

template<typename NumericalT>
DedispersionPipeline<NumericalT>::~DedispersionPipeline()
{
}

template<typename NumericalT>
void DedispersionPipeline<NumericalT>::operator()(TimeFrequencyType& data, data::AreciboConfigurationData& meta) 
{
    BaseT::operator()(data, meta); // sync call
    _sps(data);
}

template<typename NumericalT>
DedispersionPipeline<NumericalT>::DedispersionHandler::DedispersionHandler(DedispersionPipeline<NumericalT>& p)
    : _pipeline(p)
{
}

template<typename NumericalT>
void DedispersionPipeline<NumericalT>::DedispersionHandler::operator()(std::shared_ptr<DmTrialType> const&) const
{
}

template<typename NumericalT>
DedispersionPipeline<NumericalT>::SpsHandler::SpsHandler(DedispersionPipeline<NumericalT>& p)
    : _pipeline(p)
{
}

template<typename NumericalT>
void DedispersionPipeline<NumericalT>::SpsHandler::operator()(std::shared_ptr<SpType> const& data) const
{
    _pipeline.out().send(ska::panda::ChannelId("sps"), *data);
}

} // namespace pipelins 
} // namespace alfaburst
} // namespace arecibo
