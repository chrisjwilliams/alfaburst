#ifndef ARECIBO_ALFABURST_PIPELINE_PROCESSINGPIPELINE_H
#define ARECIBO_ALFABURST_PIPELINE_PROCESSINGPIPELINE_H

#include "arecibo/data/AreciboConfigurationData.h"
#include "arecibo/data/TimeFrequency.h"
#include "arecibo/sink/Factory.h"
#include "arecibo/data/TimeFrequency.h"
#include "panda/Pipeline.h"
#include <memory>
#include <tuple>

namespace arecibo {
namespace alfaburst {
namespace pipeline {


/**
 * @brief
 *   Base class for arecibo compute pipelines
 *
 */
template<typename NumericalT>
class ProcessingPipeline
{
    public:
        typedef sink::Factory<NumericalT> Exporter;
        typedef data::TimeFrequency<Cpu, NumericalT> TimeFrequencyType;

    public:
        ProcessingPipeline(Exporter&);
        virtual ~ProcessingPipeline() = 0;

        virtual void operator()(TimeFrequencyType&, data::AreciboConfigurationData&) =0;

        inline Exporter& out() { return _out; }

    private:
        Exporter& _out;
};

template<typename InputStreamType, typename PipelineType>
int exec_pipeline(InputStreamType& data_stream, PipelineType& runtime_handler)
{
    typedef typename std::tuple_element<0, typename InputStreamType::DataSetType>::type TimeFrequencyDataType;
    return ska::panda::Pipeline<InputStreamType>(data_stream, [&](TimeFrequencyDataType& d1, data::AreciboConfigurationData& d2){ runtime_handler(d1, d2); } ).exec();
}

} // namespace pipeline
} // namespace alfaburst
} // namespace arecibo
#include "detail/ProcessingPipeline.cpp"

#endif // ARECIBO_ALFABURST_PIPELINE_PROCESSINGPIPELINE_H
