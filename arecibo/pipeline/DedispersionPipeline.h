#ifndef ARECIBO_ALFABURST_PIPELINE_DEDISPERSIONPIPELINE_H
#define ARECIBO_ALFABURST_PIPELINE_DEDISPERSIONPIPELINE_H

#include "RfimPipeline.h"
#include "cheetah/sps/Sps.h"

namespace arecibo {
namespace alfaburst {
namespace pipeline {

/**
 * @brief
 *    Template class for an Dedispersion pipeline
 * @details
 * 
 */
template<typename NumericalT>
class DedispersionPipeline : public RfimPipeline<NumericalT>
{
        typedef RfimPipeline<NumericalT> BaseT;
        typedef typename BaseT::TimeFrequencyType TimeFrequencyType;

    public:
        DedispersionPipeline(AlfaBurstConfiguration const&, typename BaseT::Exporter&);
        DedispersionPipeline(DedispersionPipeline&&);
        ~DedispersionPipeline();

        void operator()(TimeFrequencyType&, data::AreciboConfigurationData&) override;

    private:
        typedef ska::cheetah::sps::Sps<ska::cheetah::sps::ConfigType<AlfaBurstConfiguration::PoolManagerType>, NumericalT> Sps;
        typedef typename Sps::DmTrialType DmTrialType;
        typedef typename Sps::SpType SpType;

        class DedispersionHandler {
            public:
                DedispersionHandler(DedispersionPipeline&);
                void operator()(std::shared_ptr<DmTrialType> const&) const;
    
            private:
                DedispersionPipeline& _pipeline;
        };

        class SpsHandler {
            public:
                SpsHandler(DedispersionPipeline&);
                void operator()(std::shared_ptr<SpType> const&) const;
    
            private:
                DedispersionPipeline& _pipeline;
        };

    private:
        DedispersionHandler _dedispersion_handler;
        SpsHandler _sps_handler;
        Sps _sps;
};

} // namespace pipeline
} // namespace alfaburst
} // namespace arecibo
#include "detail/DedispersionPipeline.cpp"

#endif // ARECIBO_ALFABURST_PIPELINE_DEDISPERSIONPIPELINE_H 
