#include "arecibo/source/Config.h"


namespace arecibo {
namespace alfaburst {
namespace source {


Config::Config()
    : BaseT("sources")
{
    add(_sigproc_config);
    add(_udp_config);
}

Config::~Config()
{
}

ska::cheetah::sigproc::Config const& Config::sigproc_config() const
{
    return _sigproc_config;
}

arecibo::alfaburst::receptor::Config const& Config::receptor_config() const
{
    return _udp_config;
}

void Config::add_options(OptionsDescriptionEasyInit& )
{
}

} // namespace source
} // namespace alfaburst
} // namespace arecibo
