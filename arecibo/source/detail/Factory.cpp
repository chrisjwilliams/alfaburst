/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2018 Oxford Astrophysics
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "arecibo/source/Config.h"
#include "arecibo/receptor/AlfaBurstStream.h"
#include "arecibo/pipeline/ProcessingPipeline.h"
#include "arecibo/AlfaBurstConfiguration.h"
#include "cheetah/sigproc/SigProcFileStream.h"

namespace arecibo {
namespace alfaburst {
namespace source {

template<typename NumericalT>
Factory<NumericalT>::Factory(Config const& config)
    : _config(config)
{
}

template<typename NumericalT>
Factory<NumericalT>::~Factory()
{
}

template<typename NumericalT>
std::vector<std::string> Factory<NumericalT>::available() const
{
    return { "sigproc", "udp" };
}

template<typename NumericalT>
template<typename ComputeModule>
int Factory<NumericalT>::exec(std::string const& stream_name, ComputeModule& runtime_handler)
{
    int rv=1;
//    if(stream_name == "sigproc") {
//        ska::cheetah::sigproc::SigProcFileStream data_stream(_config.source_config().sigproc_config());
//        rv=alfaburst::pipeline::exec_pipeline(data_stream, runtime_handler);
//    }
   //else if(stream_name == "udp") {
   if(stream_name == "udp") {
        alfaburst::receptor::AlfaBurstStream<NumericalT> data_stream(_config.receptor_config());
        rv=alfaburst::pipeline::exec_pipeline(data_stream, runtime_handler);
    }
    else {
        std::cerr << "unknown source '" << stream_name << "'" << std::endl;
    }
    return rv;
}

} // namespace source
} // namespace alfaburst
} // namespace arecibo
