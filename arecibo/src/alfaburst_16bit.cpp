#include "arecibo/pipeline/Launcher.h"
#include <cstdint>

int main(int argc, char** argv)
{
    arecibo::alfaburst::pipeline::Launcher<uint16_t> launcher;
    if(int rv=launcher.parse(argc, argv)) return rv;
    return launcher.exec();
}

