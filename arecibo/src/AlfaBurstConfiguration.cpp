#include "arecibo/AlfaBurstConfiguration.h"
#include <iostream>

namespace arecibo {
namespace alfaburst {

AlfaBurstConfiguration::AlfaBurstConfiguration()
    : BaseT("alfaburst", "pipeline launcher")
    , _modules(pool_manager())
{
//    add(_emulator_config);
}

AlfaBurstConfiguration::~AlfaBurstConfiguration()
{
}

ska::cheetah::emulator::Config const& AlfaBurstConfiguration::emulator_config() const
{
    return _emulator_config;
}

typename AlfaBurstConfiguration::Engine& AlfaBurstConfiguration::engine() const
{
    return _engine;
}

void AlfaBurstConfiguration::add_options(OptionsDescriptionEasyInit& )
{
}

} // namespace alfaburst
} // namespace arecibo
