#include "arecibo/emulator/Emulator.h"
#include "arecibo/receptor/AlfaBurstPacketGenerator.h"
#include <cheetah/emulator/EmulatorAppConfig.h>
#include <cheetah/emulator/Factory.h>
#include <cheetah/emulator/Config.h>

using namespace arecibo::alfaburst;

int main(int argc, char** argv) {

    ska::cheetah::emulator::EmulatorAppConfig app_config("alfaburst_emulator", "UDP emulation for alfaburst Beamformed Time-Frequecy data");
    int rv;
    try {
        ska::cheetah::emulator::Config& config = app_config.emulator_config();
        // add dynamic items to config
        ska::cheetah::generators::GeneratorFactory<uint16_t> generator_factory(config.generators_config());
        app_config.set_generator_list(generator_factory.available());

        // parse command line options
        if( (rv=app_config.parse(argc, argv)) ) return rv;

        // setup the emulator
        typedef receptor::AlfaBurstPacketGenerator StreamType;
        typedef ska::cheetah::emulator::Factory<StreamType, uint16_t> FactoryType;
        FactoryType factory(config, generator_factory);
        std::unique_ptr<typename FactoryType::EmulatorType> emulator(factory.create(config.generator()));
        PANDA_LOG << "emulator using generator: '" << config.generator() << "'";

        // start the emulator
        return emulator->run();
    }
    catch(std::exception const& e) {
        std::cerr << "Emulator: " << e.what() << std::endl;;
    }
    catch(...) {
        std::cerr << "Emulator: unknown exception caught" << std::endl;
    }
    return 1;
}
